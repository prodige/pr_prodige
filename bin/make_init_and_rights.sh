#!/bin/bash
set -e

SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

DOCKERDIR="${SCRIPTDIR}/.."

RESET=false

# Date
DATE="`date '+%Y%m%d'`"

NO_DATA=false

####################################################################################################################

##################################
# Usage                          #
##################################

function usage {
    logOk "Init data volumes for prodige"
    logOk "Usage"
    printf "\t$SCRIPTNAME [arg...]\n"
    printf "Options :\n"
    printf "\t$SCRIPTNAME -h | --help                                       : Help\n"
    printf "\t$SCRIPTNAME --no-data                                         : Exclude recursive chown/chmod on ./data/data\n"
}

########################################################################################################################################

##################################
# Print log                      #
##################################
# Bash color
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m'
MODE="init"
function logOk {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logError {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarning {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function logOkStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logErrorStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarningStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function ifCmdFailStep {
    if [ $? -ne 0 ]; then
        logErrorStep $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}

function ifCmdFail {
    if [ $? -ne 0 ]; then
        logError $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}


########################################################################################################################################


##################################
# Function                       #
##################################

function initConfig {
    logOkStep "Init ./data with ./init/config"
    mkdir -p ${DOCKERDIR}/data
    rsync -rl --ignore-existing ${DOCKERDIR}/init/config/ ${DOCKERDIR}/data/
    chmod o+rx ${DOCKERDIR}/data
    chmod o+rx ${DOCKERDIR}/data/conf
    chmod o+r ${DOCKERDIR}/data/conf/docker.env
    chmod o+r ${DOCKERDIR}/data/conf/epsg
    chmod g+rw ${DOCKERDIR}/data/conf/epsg
    chown 33:33 ${DOCKERDIR}/data/conf/epsg

    logOkStep "Fix config rights:"
    logOkStep "  Admincarto"
    chmod o+r ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml

    logOkStep "  Adminsite"
    chmod o+r ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml

    logOkStep "  CAS-openldap"
    chmod o+rx ${DOCKERDIR}/data/openldap
    chmod o+r ${DOCKERDIR}/data/openldap/db.env

    logOkStep "  CAS"
    find ${DOCKERDIR}/data/cas -type d -exec chmod o+rx {} \;
    find ${DOCKERDIR}/data/cas -type f -exec chmod o+r {} \;
    chown -R 33:33 ${DOCKERDIR}/data/cas

    logOkStep "  Catalogue"
    chmod o+r ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml

    logOkStep "  Database"
    chmod o+rx ${DOCKERDIR}/data/database
    chmod o+r ${DOCKERDIR}/data/database/db.env
    chmod o+r ${DOCKERDIR}/data/database/postgresql.conf

    logOkStep "  Datacarto"
    chmod o+r ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml

    logOkStep "  Geonetwork"
    find ${DOCKERDIR}/data/geonetwork/conf -type d -exec chmod o+rx {} \;
    find ${DOCKERDIR}/data/geonetwork/conf -type f -exec chmod o+r {} \;
    chown -R 33:33 ${DOCKERDIR}/data/geonetwork/conf
    find ${DOCKERDIR}/data/geonetwork/WEB-INF -type d -exec chmod o+rx {} \;
    find ${DOCKERDIR}/data/geonetwork/WEB-INF -type f -exec chmod o+r {} \;
    chown -R 33:33 ${DOCKERDIR}/data/geonetwork/WEB-INF

    logOkStep "  MapserverApi"
    chmod o+r ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml

    logOkStep "  Proxy"
    chmod o+r ${DOCKERDIR}/data/proxy/ssl/*.crt

    logOkStep "  Table"
    chmod o+r ${DOCKERDIR}/data/table/conf/env.js

    logOkStep "  Telecarto"
    chmod o+r ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml

    logOkStep "  Visualiseur"
    chmod o+r ${DOCKERDIR}/data/visualiseur/conf/env.js

    logOkStep "  Visualiseur-back"
    chmod o+r ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml

    logOkStep "  Visualiseur-core-print"
    chmod o+r ${DOCKERDIR}/data/visualiseur-core-print/conf/env.js
}

function initData {
    logOkStep "Init ./data/data with ./init/data/data"
    mkdir -p ${DOCKERDIR}/data/data
    rsync -rl --ignore-existing ${DOCKERDIR}/init/data/data/ ${DOCKERDIR}/data/data/
    mkdir -p ${DOCKERDIR}/data/data/QUEUE
    mkdir -p ${DOCKERDIR}/data/data/ldif
    mkdir -p ${DOCKERDIR}/data/data/atomdata
    mkdir -p ${DOCKERDIR}/data/data/savedcontext

    if $NO_DATA
    then
        logOkStep "Don't fix rights in ./data/data"
    else
        logOkStep "Fix rights in ./data/data"
        find ${DOCKERDIR}/data/data -type d -exec chmod 0775 {} \;
        find ${DOCKERDIR}/data/data -type f -exec chmod 0664 {} \;
        chown -R 33:33 ${DOCKERDIR}/data/data
    fi

    logOkStep "Init ./data/share with ./init/data/share"
    mkdir -p ${DOCKERDIR}/data/share
    rsync -rl --ignore-existing ${DOCKERDIR}/init/data/share/ ${DOCKERDIR}/data/share/
    chmod o+r ${DOCKERDIR}/data/share/*

    logOkStep "Init ./data - create empty directories"
    logOkStep "  CAS-openldap"
    mkdir -p ${DOCKERDIR}/data/openldap/config

    logOkStep "  Catalogue"
    mkdir -p ${DOCKERDIR}/data/catalogue/upload
    chmod o+rwx ${DOCKERDIR}/data/catalogue/upload

    mkdir -p ${DOCKERDIR}/data/catalogue/upload/ressources
    chmod o+rwx ${DOCKERDIR}/data/catalogue/upload/ressources

    mkdir -p ${DOCKERDIR}/data/catalogue/thumbnails
    chmod o+rwx ${DOCKERDIR}/data/catalogue/thumbnails

    logOkStep "Init ./data/geonetwork with ./init/data/geonetwork"
    rsync -rl --ignore-existing ${DOCKERDIR}/init/data/geonetwork/data/ ${DOCKERDIR}/data/geonetwork/data/
    chown -R 33:33 ${DOCKERDIR}/data/geonetwork/data
    find ${DOCKERDIR}/data/geonetwork/data -type d -exec chmod o+rx {} \;
    find ${DOCKERDIR}/data/geonetwork/data -type f -exec chmod o+r {} \;
    rsync -rl --ignore-existing ${DOCKERDIR}/init/data/geonetwork/codelist/ ${DOCKERDIR}/data/geonetwork/codelist/
    chown -R 33:33 ${DOCKERDIR}/data/geonetwork/codelist
    find ${DOCKERDIR}/data/geonetwork/codelist -type d -exec chmod o+rx {} \;
    find ${DOCKERDIR}/data/geonetwork/codelist -type f -exec chmod o+r {} \;

}

function initVar {
    logOkStep "Init ./var :"
    mkdir -p ${DOCKERDIR}/var

    logOkStep "  Admincarto"
    mkdir -p ${DOCKERDIR}/var/admincarto/log_symfony
    chown 33:33 ${DOCKERDIR}/var/admincarto/log_symfony
    mkdir -p ${DOCKERDIR}/var/admincarto/log

    logOkStep "  Adminsite"
    mkdir -p ${DOCKERDIR}/var/adminsite/log_symfony
    chown 33:33 ${DOCKERDIR}/var/adminsite/log_symfony
    mkdir -p ${DOCKERDIR}/var/adminsite/log

    logOkStep "  CAS-openldap"
    mkdir -p ${DOCKERDIR}/var/openldap/database
    mkdir -p ${DOCKERDIR}/var/openldap/restore
    find ${DOCKERDIR}/var/openldap/restore -type d -exec chmod o+rx {} \;
    find ${DOCKERDIR}/var/openldap/restore -type f -exec chmod o+r {} \;

    logOkStep "  CAS"
    mkdir -p ${DOCKERDIR}/var/cas/log
    chown 33:33 ${DOCKERDIR}/var/cas/log

    logOkStep "  Catalogue"
    mkdir -p ${DOCKERDIR}/var/catalogue/log_symfony
    chown 33:33 ${DOCKERDIR}/var/catalogue/log_symfony
    mkdir -p ${DOCKERDIR}/var/catalogue/log

    logOkStep "  Database"
    mkdir -p ${DOCKERDIR}/var/database/data
    mkdir -p ${DOCKERDIR}/var/database/restore
    find ${DOCKERDIR}/var/database/restore -type d -exec chmod o+rx {} \;
    find ${DOCKERDIR}/var/database/restore -type f -exec chmod o+r {} \;

    logOkStep "  Datacarto"
    mkdir -p ${DOCKERDIR}/var/datacarto/tiles
    chown -R 33:33 ${DOCKERDIR}/var/datacarto/tiles
    mkdir -p ${DOCKERDIR}/var/datacarto/log_phalcon
    chown 33:33 ${DOCKERDIR}/var/datacarto/log_phalcon
    mkdir -p ${DOCKERDIR}/var/datacarto/log

    logOkStep "  Geonetwork"
    mkdir -p ${DOCKERDIR}/var/geonetwork/log
    chown 33:33 ${DOCKERDIR}/var/geonetwork/log

    logOkStep "  MapserverApi"
    mkdir -p ${DOCKERDIR}/var/mapserver/log_symfony
    chown 33:33 ${DOCKERDIR}/var/mapserver/log_symfony
    mkdir -p ${DOCKERDIR}/var/mapserver/log

    logOkStep "  Table"
    mkdir -p ${DOCKERDIR}/var/table/log

    logOkStep "  Telecarto"
    mkdir -p ${DOCKERDIR}/var/telecarto/DOWNLOAD
    chown 33:33 ${DOCKERDIR}/var/telecarto/DOWNLOAD
    mkdir -p ${DOCKERDIR}/var/telecarto/log_symfony
    chown 33:33 ${DOCKERDIR}/var/telecarto/log_symfony
    mkdir -p ${DOCKERDIR}/var/telecarto/log

    logOkStep "  Visualiseur"
    mkdir -p ${DOCKERDIR}/var/visualiseur/log

    logOkStep "  Visualiseur-back"
    mkdir -p ${DOCKERDIR}/var/visualiseur-back/sessions
    chown www-data: ${DOCKERDIR}/var/visualiseur-back/sessions
    chmod 1733 ${DOCKERDIR}/var/visualiseur-back/sessions
    mkdir -p ${DOCKERDIR}/var/visualiseur-back/log_cas
    chown 33:33 ${DOCKERDIR}/var/visualiseur-back/log_cas
    mkdir -p ${DOCKERDIR}/var/visualiseur-back/log

    logOkStep "  Visualiseur-core-print"
    mkdir -p ${DOCKERDIR}/var/visualiseur-core-print/log

}

########################################################################################################################################

##################################
# main                           #
##################################

MODE="main"
logOk "############################"
logOk "########### main ###########"
logOk "############################"
# Options
OPTS=$( getopt -o h -l help,no-data -- "$@" )
ifCmdFail "Option not found, use \"$SCRIPT -h\""
eval set -- "$OPTS"
while true
do
    case "$1" in
        -h | --help)
        usage
        exit 0
        ;;
        --no-data)
        NO_DATA=true
        shift
        ;;
        --)
        shift
        break
        ;;
    esac
done

if [[ $(id -u) -ne 0 ]] ; then
    logError "You must be root or sudo to execute $SCRIPTNAME"
    exit 1
fi

# Step
NB_STEP_AL=3

STEP_AL=1
MODE="InitConfig"
logOkStep "Init config in ./data"
initConfig

STEP_AL=2
MODE="InitData"
logOkStep "Init data in ./data"
initData

STEP_AL=3
MODE="InitVar"
logOkStep "Init var in ./var"
initVar

logOk "End of ${SCRIPTNAME}"
# End
