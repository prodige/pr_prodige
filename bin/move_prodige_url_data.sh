#!/bin/bash

SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

DOCKERDIR=${SCRIPTDIR}/..

YES=false

# Date
DATE="`date '+%Y%m%d'`"

# URL
DOMAIN=".prodige.internal"
ADMINCARTO="admincarto"
ADMINSITE="adminsite"
CARTO="carto"
CAS="cas"
CATALOGUE="catalogue"
DATACARTO="datacarto"
MAPSERV="mapserv"
PRINT="print"
TABLE="table"
TELECARTO="telecarto"
# OLD URL
OLDDOMAIN=".prodige.internal"
OLDADMINCARTO="admincarto"
OLDADMINSITE="adminsite"
OLDCARTO="carto"
OLDCAS="cas"
OLDCATALOGUE="catalogue"
OLDDATACARTO="datacarto"
OLDMAPSERV="mapserv"
OLDPRINT="print"
OLDTABLE="table"
OLDTELECARTO="telecarto"

NO_DATA=false

####################################################################################################################

##################################
# Usage                          #
##################################

function usage {
    logOk "Usage"
    printf "\t$SCRIPTNAME [arg...]\n"
    printf "Options :\n"
    printf "\t$SCRIPTNAME -h | --help                                       : Help\n"
    printf "\t$SCRIPTNAME -d <domain> | --domain                            : Domain of prodige. default: .prodige.internal (ex: .alkante.com or -test.alkante.com) \n"
    printf "\t$SCRIPTNAME -a <admincarto> | --admincarto                    : Subdomain of admincarto (default: admincarto)\n"
    printf "\t$SCRIPTNAME -s <adminsite> | --adminsite                      : Subdomain of adminsite (default: adminsite)\n"
    printf "\t$SCRIPTNAME -r <carto> | --carto                              : Subdomain of carto (default: carto)\n"
    printf "\t$SCRIPTNAME -k <cas> | --cas                                  : Subdomain of cas (default: cas)\n"
    printf "\t$SCRIPTNAME -c <catalogue> | --catalogue                      : Subdomain of catalogue (default: catalogue)\n"
    printf "\t$SCRIPTNAME -q <datacarto> | --datacarto                      : Subdomain of datacarto (default: datacarto)\n"
    printf "\t$SCRIPTNAME -m <mapserv> | --mapserv                          : Subdomain of mapserv (default: mapserv)\n"
    printf "\t$SCRIPTNAME -p <print> | --print                              : Subdomain of print (default: print)\n"
    printf "\t$SCRIPTNAME -t <table> | --table                              : Subdomain of table (default: table)\n"
    printf "\t$SCRIPTNAME -e <telecarto> | --telecarto                      : Subdomain of telecarto (default: telecarto)\n"
    printf "\t$SCRIPTNAME -y | --yes                                        : Non-interactive mode\n"
    printf "\t$SCRIPTNAME -D <olddomain> | --olddomain                      : Old domain of prodige. default: .prodige.internal (ex: .alkante.com or -test.alkante.com) \n"
    printf "\t$SCRIPTNAME -A <oldadmincarto> | --oldadmincarto              : Old subdomain of admincarto (default: admincarto)\n"
    printf "\t$SCRIPTNAME -S <oldadminsite> | --oldadminsite                : Old subdomain of adminsite (default: adminsite)\n"
    printf "\t$SCRIPTNAME -R <oldcarto> | --oldcarto                        : Old subdomain of carto (default: carto)\n"
    printf "\t$SCRIPTNAME -K <oldcas> | --oldcas                            : Old subdomain of cas (default: cas)\n"
    printf "\t$SCRIPTNAME -C <oldcatalogue> | --oldcatalogue                : Old subdomain of catalogue (default: catalogue)\n"
    printf "\t$SCRIPTNAME -Q <olddatacarto> | --olddatacarto                : Old subdomain of datacarto (default: datacarto)\n"
    printf "\t$SCRIPTNAME -M <oldmapserv> | --oldmapserv                    : Old subdomain of mapserv (default: mapserv)\n"
    printf "\t$SCRIPTNAME -P <oldprint> | --oldprint                        : Old subdomain of print (default: print)\n"
    printf "\t$SCRIPTNAME -T <oldtable> | --oldtable                        : Old subdomain of table (default: table)\n"
    printf "\t$SCRIPTNAME -E <oldtelecarto> | --oldtelecarto                : Old subdomain of telecarto (default: telecarto)\n"
    printf "\t$SCRIPTNAME --no-data                                         : Exclude recursive chown/chmod on ./data/data\n"
}

########################################################################################################################################

##################################
# Print log                      #
##################################
# Bash color
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m'
MODE="init"
function logOk {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logError {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarning {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function logOkStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logErrorStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarningStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function ifCmdFailStep {
    if [ $? -ne 0 ]; then
        logErrorStep $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}

function ifCmdFail {
    if [ $? -ne 0 ]; then
        logError $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}


########################################################################################################################################


##################################
# Function                       #
##################################

function change_variables {
    logOkStep "If no change, press enter"
    printf "Set domain : ${DOMAIN} -> "
    read
    if [[ $REPLY != '' ]]
    then
       DOMAIN="$REPLY"
    fi
    printf "Set admincarto : ${ADMINCARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       ADMINCARTO="$REPLY"
    fi
    printf "Set adminsite : ${ADMINSITE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       ADMINSITE="$REPLY"
    fi
    printf "Set carto : ${CARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       CARTO="$REPLY"
    fi
    printf "Set cas : ${CAS} -> "
    read
    if [[ $REPLY != '' ]]
    then
       CAS="$REPLY"
    fi
    printf "Set catalogue : ${CATALOGUE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       CATALOGUE="$REPLY"
    fi
    printf "Set datacarto : ${DATACARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       DATACARTO="$REPLY"
    fi
    printf "Set mapserv : ${MAPSERV} -> "
    read
    if [[ $REPLY != '' ]]
    then
       MAPSERV="$REPLY"
    fi
    printf "Set print : ${PRINT} -> "
    read
    if [[ $REPLY != '' ]]
    then
       PRINT="$REPLY"
    fi
    printf "Set table : ${TABLE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       TABLE="$REPLY"
    fi
    printf "Set telecarto : ${TELECARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       TELECARTO="$REPLY"
    fi
    # OLD variables
    printf "Set old domain : ${OLDDOMAIN} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDDOMAIN="$REPLY"
    fi
    printf "Set old admincarto : ${OLDADMINCARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDADMINCARTO="$REPLY"
    fi
    printf "Set old adminsite : ${OLDADMINSITE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDADMINSITE="$REPLY"
    fi
    printf "Set old carto : ${OLDCARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDCARTO="$REPLY"
    fi
    printf "Set old cas : ${OLDCAS} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDCAS="$REPLY"
    fi
    printf "Set old catalogue : ${OLDCATALOGUE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDCATALOGUE="$REPLY"
    fi
    printf "Set old datacarto : ${OLDDATACARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDDATACARTO="$REPLY"
    fi
    printf "Set old mapserv : ${OLDMAPSERV} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDMAPSERV="$REPLY"
    fi
    printf "Set old print : ${OLDPRINT} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDPRINT="$REPLY"
    fi
    printf "Set old table : ${OLDTABLE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDTABLE="$REPLY"
    fi
    printf "Set old telecarto : ${OLDTELECARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDTELECARTO="$REPLY"
    fi
    validation
}

function validation {
    logOkStep "Validation : "
    echo "domain    : ${DOMAIN}"
    echo "subdomain :"
    echo "    admincarto : ${ADMINCARTO}${DOMAIN}"
    echo "    adminsite  : ${ADMINSITE}${DOMAIN}"
    echo "    carto      : ${CARTO}${DOMAIN}"
    echo "    cas        : ${CAS}${DOMAIN}"
    echo "    catalogue  : ${CATALOGUE}${DOMAIN}"
    echo "    datacarto  : ${DATACARTO}${DOMAIN}"
    echo "    mapserv    : ${MAPSERV}${DOMAIN}"
    echo "    print      : ${PRINT}${DOMAIN}"
    echo "    table      : ${TABLE}${DOMAIN}"
    echo "    telecarto  : ${TELECARTO}${DOMAIN}"
    echo "old domain    : ${OLDDOMAIN}"
    echo "old subdomain :"
    echo "    old admincarto : ${OLDADMINCARTO}${OLDDOMAIN}"
    echo "    old adminsite  : ${OLDADMINSITE}${OLDDOMAIN}"
    echo "    old carto      : ${OLDCARTO}${OLDDOMAIN}"
    echo "    old cas        : ${OLDCAS}${OLDDOMAIN}"
    echo "    old catalogue  : ${OLDCATALOGUE}${OLDDOMAIN}"
    echo "    old datacarto  : ${OLDDATACARTO}${OLDDOMAIN}"
    echo "    old mapserv    : ${OLDMAPSERV}${OLDDOMAIN}"
    echo "    old print      : ${OLDPRINT}${OLDDOMAIN}"
    echo "    old table      : ${OLDTABLE}${OLDDOMAIN}"
    echo "    old telecarto  : ${OLDTELECARTO}${OLDDOMAIN}"
    if ! ${YES}
    then
       logOkStep "Are these variables valid?  (y/n)"
       read
    fi
    if [[ $REPLY =~ ^[Yy]$ ]] || ${YES}
    then
       logOkStep "Continue script"
    else
       logOkStep "Change variables"
       change_variables
    fi

}

function get_db_parameters {
    logOkStep "Get parameters from configuration files"
    PARAM_FILE="${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml"
    user_bdd=$(eval echo $(awk '/catalogue_user/ { print $2 }' ${PARAM_FILE}))
    pass_bdd=$(eval echo $(awk '/catalogue_password/ { print $2 }' ${PARAM_FILE}))
    # sur prodige CATALOGUE="CATALOGUE" / puis CATALOGUE=alk_respire
    CATALOGUE_DB=$(eval echo $(awk '/catalogue_name/ { print $2 }' ${PARAM_FILE}))
    PRODIGE_DB=$(eval echo $(awk '/prodige_name/ { print $2 }' ${PARAM_FILE}))
    host_bdd=$(eval echo $(awk '/prodige_host/ { print $2 }' ${PARAM_FILE}))
    echo "user_bdd=$user_bdd"
    echo "pass_bdd=$pass_bdd"
    echo "CATALOGUE_DB=$CATALOGUE_DB"
    echo "PRODIGE_DB=$PRODIGE_DB"
    echo "host_bdd=$host_bdd"
}

function change_cartes {
    if $NO_DATA
    then
        logOkStep "Don't fix files in ./data/data"
    else
        logOkStep "Updating .map and .xml files in /data/data/cartes with new parameters (domains and DB connection)"
        find ${DOCKERDIR}/data/data/cartes \( -name "*.map" -o -name "*.xml" \) -print0 | xargs -0 sed -i "
s/CONNECTION \"user=.*dbname=.*\"/CONNECTION \"user=${user_bdd} password=${pass_bdd} dbname=${PRODIGE_DB} host=${host_bdd} port=5432\"/g
s/${OLDADMINCARTO}${OLDDOMAIN}/${ADMINCARTO}${DOMAIN}/g
s/${OLDADMINSITE}${OLDDOMAIN}/${ADMINSITE}${DOMAIN}/g
s/${OLDCARTO}${OLDDOMAIN}/${CARTO}${DOMAIN}/g
s/${OLDCAS}${OLDDOMAIN}/${CAS}${DOMAIN}/g
s/${OLDCATALOGUE}${OLDDOMAIN}/${CATALOGUE}${DOMAIN}/g
s/${OLDDATACARTO}${OLDDOMAIN}/${DATACARTO}${DOMAIN}/g
s/${OLDMAPSERV}${OLDDOMAIN}/${MAPSERV}${DOMAIN}/g
s/${OLDPRINT}${OLDDOMAIN}/${PRINT}${DOMAIN}/g
s/${OLDTABLE}${OLDDOMAIN}/${TABLE}${DOMAIN}/g
s/${OLDTELECARTO}${OLDDOMAIN}/${TELECARTO}${DOMAIN}/g
"
    fi
}

function change_rdf {
    logOkStep "Updating .rdf files in /data/geonetwork with new domains"
    find ${DOCKERDIR}/data/geonetwork/ -type f -name "*.rdf" -print0 | xargs -0 -r sed -i "
s/${OLDADMINCARTO}${OLDDOMAIN}/${ADMINCARTO}${DOMAIN}/g
s/${OLDADMINSITE}${OLDDOMAIN}/${ADMINSITE}${DOMAIN}/g
s/${OLDCARTO}${OLDDOMAIN}/${CARTO}${DOMAIN}/g
s/${OLDCAS}${OLDDOMAIN}/${CAS}${DOMAIN}/g
s/${OLDCATALOGUE}${OLDDOMAIN}/${CATALOGUE}${DOMAIN}/g
s/${OLDDATACARTO}${OLDDOMAIN}/${DATACARTO}${DOMAIN}/g
s/${OLDMAPSERV}${OLDDOMAIN}/${MAPSERV}${DOMAIN}/g
s/${OLDPRINT}${OLDDOMAIN}/${PRINT}${DOMAIN}/g
s/${OLDTABLE}${OLDDOMAIN}/${TABLE}${DOMAIN}/g
s/${OLDTELECARTO}${OLDDOMAIN}/${TELECARTO}${DOMAIN}/g
"
    # some substitutions never occur (determine and remove)
}

function set_services {
    if $NO_DATA
    then
        logOkStep "Don't fix services in ./data/data"
    else
        logOkStep "Set data/data/SYSTEM/services.xml"
        TRAILINGDOMAIN=${DOMAIN:1:${#DOMAIN}}
        sed -i "s|^\(.*dns=\"\)[^\"]*\(.*\)$|\1${TRAILINGDOMAIN}\2|" ${DOCKERDIR}/data/data/SYSTEM/services.xml
        sed -i "s|^\(.*prefixBackoffice=\"\)[^\"]*\(.*\)$|\1${ADMINCARTO}\2|" ${DOCKERDIR}/data/data/SYSTEM/services.xml
        sed -i "s|^\(.*prefixData=\"\)[^\"]*\(.*\)$|\1${DATACARTO}\2|" ${DOCKERDIR}/data/data/SYSTEM/services.xml
        sed -i "s|^\(.*prefixFrontffice=\"\)[^\"]*\(.*\)$|\1${CARTO}\2|" ${DOCKERDIR}/data/data/SYSTEM/services.xml
    fi
}

function generate_owscontext {
    logOkStep "Checking ppr_prodige_admincarto_web docker is running"
    [ "$( docker container inspect -f '{{.State.Status}}' ppr_prodige_admincarto_web)" == "running" ]
    ifCmdFailStep "ppr_prodige_admincarto_web container is not running. Please start appropriate docker-compose."
    logOkStep "Generate owscontext by calling console in docker ppr_prodige_admincarto_web"
    docker exec -u www-data -w /var/www/html/site ppr_prodige_admincarto_web php bin/console carmen:generateContext 1
}

function clear_metadata_subversion {
    logOkStep "Clearing data/geonetwork/data/metadata_subversion/ directory"
    rm -rf ${DOCKERDIR}/data/geonetwork/data/metadata_subversion/
}

function change_data {
    get_db_parameters
    change_cartes
    change_rdf
    clear_metadata_subversion
    set_services
    generate_owscontext
}
########################################################################################################################################

##################################
# main                           #
##################################

MODE="main"
logOk "############################"
logOk "########### main ###########"
logOk "############################"
# Options
OPTS=$( getopt -o h,d:,a:,s:,r:,k:,c:,q:,m:,p:,t:,e:,y,D:,A:,S:,R:,K:,C:,Q:,M:,P:,T:,E: -l help,domain:,admincarto:,adminsite:,carto:,cas:,catalogue:,datacarto:,mapserv:,print:,table:,telecarto:,yes,olddomain:,oldadmincarto:,oldadminsite:,oldcarto:,oldcas:,oldcatalogue:,olddatacarto:,oldmapserv:,oldprint:,oldtable:,oldtelecarto:,no-data -- "$@" )
ifCmdFail "Option not found, use \"$SCRIPT -h\""
eval set -- "$OPTS"
while true
do
    case "$1" in
        -h | --help)
        usage
        exit 0
        ;;
        -d | --domain)
        DOMAIN=$2
        shift 2
        ;;
        -a | --admincarto)
        ADMINCARTO=$2
        shift 2
        ;;
        -s | --adminsite)
        ADMINSITE=$2
        shift 2
        ;;
        -r | --carto)
        CARTO=$2
        shift 2
        ;;
        -k | --cas)
        CAS=$2
        shift 2
        ;;
        -c | --catalogue)
        CATALOGUE=$2
        shift 2
        ;;
        -q | --datacarto)
        DATACARTO=$2
        shift 2
        ;;
        -m | --mapserv)
        MAPSERV=$2
        shift 2
        ;;
        -p | --print)
        PRINT=$2
        shift 2
        ;;
        -t | --table)
        TABLE=$2
        shift 2
        ;;
        -e | --telecarto)
        TELECARTO=$2
        shift 2
        ;;
        -y | --yes)
        YES=true
        shift
        ;;
        -D | --olddomain)
        OLDDOMAIN=$2
        shift 2
        ;;
        -A | --oldadmincarto)
        OLDADMINCARTO=$2
        shift 2
        ;;
        -S | --oldadminsite)
        OLDADMINSITE=$2
        shift 2
        ;;
        -R | --oldcarto)
        OLDCARTO=$2
        shift 2
        ;;
        -K | --oldcas)
        OLDCAS=$2
        shift 2
        ;;
        -C | --oldcatalogue)
        OLDCATALOGUE=$2
        shift 2
        ;;
        -Q | --olddatacarto)
        OLDDATACARTO=$2
        shift 2
        ;;
        -M | --oldmapserv)
        OLDMAPSERV=$2
        shift 2
        ;;
        -P | --oldprint)
        OLDPRINT=$2
        shift 2
        ;;
        -T | --oldtable)
        OLDTABLE=$2
        shift 2
        ;;
        -E | --oldtelecarto)
        OLDTELECARTO=$2
        shift 2
        ;;
        --no-data)
        NO_DATA=true
        shift
        ;;
        --)
        shift
        break
        ;;
    esac
done

if [[ $(id -u) -ne 0 ]] ; then
    logError "You must be root or sudo to execute $SCRIPTNAME"
    exit 1
fi

# Step
NB_STEP_AL=2

STEP_AL=1
MODE="Validation"
logOkStep "Validation of variables"
validation

STEP_AL=2
MODE="SetParameters"
logOkStep "Set parameters"
change_data

logOk "End of ${SCRIPTNAME}"
# End
