#!/bin/bash
set -e
SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

DOCKERDIR="${SCRIPTDIR}/.."

YES=false

# Date
DATE="`date '+%Y%m%d'`"

# parameters
USER_NAME="user_prodige"
USER_PASSWORD="databasepassword"
POSTGRES_PASSWORD="alkante"
DB_PRODIGE_NAME="prodige"
DB_CATALOGUE_NAME="catalogue"
LDAP_ADMIN_PASSWORD="ldappassword"
LDAP_CONFIG_PASSWORD="ldapconfigpassword"
LDAP_ADMINCLI_PASSWORD="clipassword"
SMTP_RELAY=`hostname -f`
SMTP_MYHOSTNAME=''
SMTP_MYDOMAIN=''
DEBUG_MAIL="debug@prodige-opensource.org"
TIMEZONE="Europe/Paris"
DOUBLEAUTH=false

####################################################################################################################

##################################
# Usage                          #
##################################

function usage {
    logOk "Usage"
    printf "\t$SCRIPTNAME [arg...]\n"
    printf "Options :\n"
    printf "\t$SCRIPTNAME -h | --help                                            : Help\n"
    printf "\t$SCRIPTNAME -u <user_name> | --user                                : User of prodige and catalogue databases (default: user_prodige)\n"
    printf "\t$SCRIPTNAME -w <user_password> | --password                        : Password of databases user (default: databasepassword)\n"
    printf "\t$SCRIPTNAME -z <postgres_password> | --postgres-password           : Password of postgres user (default: alkante)\n"
    printf "\t$SCRIPTNAME -g <db_prodige_name> | --db-prodige-name               : Name of prodige database (default: prodige)\n"
    printf "\t$SCRIPTNAME -l <db_catalogue_name> | --db-catalogue-name           : Name of catalogue database (default: catalogue)\n"
    printf "\t$SCRIPTNAME -x <ldap_admin_password> | --ldap-admin-password       : LDAP admin password (default: ldappassword)\n"
    printf "\t$SCRIPTNAME -f <ldap_config_password> | --ldap-config-password     : LDAP config password (default: ldapconfigpassword)\n"
    printf "\t$SCRIPTNAME -i <ldap_admincli_password> | --ldap-admincli-password : LDAP admincli password (default: clipassword)\n"
    printf "\t$SCRIPTNAME -j <relay_smtp> | --smtp-relay                         : SMTP relay (default: $SMTP_RELAY), more detail : https://postfix.traduc.org/index.php/postconf.5.html#relayhost\n"
    printf "\t$SCRIPTNAME --smtp-myhostname <host smtp>                          : SMTP myhostname (default: docker id), more detail : https://postfix.traduc.org/index.php/postconf.5.html#myhostname\n"
    printf "\t$SCRIPTNAME --smtp-mydomain <domain smtp>                          : SMTP mydomain (default: docker domain), more detail : https://postfix.traduc.org/index.php/postconf.5.html#mydomain\n"
    printf "\t$SCRIPTNAME -b <debug_mail> | --debug-mail                         : In case of problem, send an email to (default: debug@prodige-opensource.org)\n"
    printf "\t$SCRIPTNAME -o <timezone> | --timezone                             : Timezone (default: Europe/Paris)\n"
    printf "\t$SCRIPTNAME -A | --double-auth                                     : Enable double authentification (email)\n"
    printf "\t$SCRIPTNAME -y | --yes                                             : Non-interactive mode\n"
}

########################################################################################################################################

##################################
# Print log                      #
##################################
# Bash color
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m'
MODE="init"
function logOk {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logError {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarning {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function logOkStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logErrorStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarningStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function ifCmdFailStep {
    if [ $? -ne 0 ]; then
        logErrorStep $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}

function ifCmdFail {
    if [ $? -ne 0 ]; then
        logError $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}


########################################################################################################################################


##################################
# Function                       #
##################################

function change_variables {
    logOkStep "If no change, press enter"
    printf "Set user_name: ${USER_NAME} -> "
    read
    if [[ $REPLY != '' ]]
    then
       USER_NAME="$REPLY"
    fi
    printf "Set user_password: ${USER_PASSWORD} -> "
    read
    if [[ $REPLY != '' ]]
    then
       USER_PASSWORD="$REPLY"
    fi
    printf "Set postgres_password: ${POSTGRES_PASSWORD} -> "
    read
    if [[ $REPLY != '' ]]
    then
       POSTGRES_PASSWORD="$REPLY"
    fi
    printf "Set db_prodige_name: ${DB_PRODIGE_NAME} -> "
    read
    if [[ $REPLY != '' ]]
    then
       DB_PRODIGE_NAME="$REPLY"
    fi
    printf "Set db_catalogue_name: ${DB_CATALOGUE_NAME} -> "
    read
    if [[ $REPLY != '' ]]
    then
       DB_CATALOGUE_NAME="$REPLY"
    fi
    printf "Set ldap_admin_password: ${LDAP_ADMIN_PASSWORD} -> "
    read
    if [[ $REPLY != '' ]]
    then
       LDAP_ADMIN_PASSWORD="$REPLY"
    fi
    printf "Set ldap_config_password: ${LDAP_CONFIG_PASSWORD} -> "
    read
    if [[ $REPLY != '' ]]
    then
       LDAP_CONFIG_PASSWORD="$REPLY"
    fi
    printf "Set ldap_admincli_password: ${LDAP_ADMINCLI_PASSWORD} -> "
    read
    if [[ $REPLY != '' ]]
    then
       LDAP_ADMINCLI_PASSWORD="$REPLY"
    fi
    printf "Set smtp_relay: ${SMTP_RELAY} -> "
    read
    if [[ $REPLY != '' ]]
    then
       SMTP_RELAY="$REPLY"
    fi
    printf "Set smtp_myhostname : ${SMTP_MYHOSTNAME} -> "
    read
    if [[ $REPLY != '' ]]
    then
       SMTP_MYHOSTNAME="$REPLY"
    fi
    printf "Set smtp_mydomain: ${SMTP_MYDOMAIN} -> "
    read
    if [[ $REPLY != '' ]]
    then
       SMTP_MYDOMAIN="$REPLY"
    fi
    printf "Set debug_mail: ${DEBUG_MAIL} -> "
    read
    if [[ $REPLY != '' ]]
    then
       DEBUG_MAIL="$REPLY"
    fi
    printf "Set timezone: ${TIMEZONE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       TIMEZONE="$REPLY"
    fi
    printf "Enable double auth: (y/N) -> "
    read
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
      DOUBLEAUTH=true
    else
      DOUBLEAUTH=false
    fi
    validation
}

function validation {
    logOkStep "Validation: "
    echo "user_prodige          : ${USER_NAME}"
    echo "user_password         : ${USER_PASSWORD}"
    echo "postgres_password     : ${POSTGRES_PASSWORD}"
    echo "db_prodige_name       : ${DB_PRODIGE_NAME}"
    echo "db_catalogue_name     : ${DB_CATALOGUE_NAME}"
    echo "ldap_admin_password   : ${LDAP_ADMIN_PASSWORD}"
    echo "ldap_config_password  : ${LDAP_CONFIG_PASSWORD}"
    echo "ldap_admincli_password: ${LDAP_ADMINCLI_PASSWORD}"
    echo "smtp_relay            : ${SMTP_RELAY}"
    echo "smtp_myhostname       : ${SMTP_MYHOSTNAME}"
    echo "smtp_mydomain         : ${SMTP_MYDOMAIN}"
    echo "debug_mail            : ${DEBUG_MAIL}"
    echo "timezone              : ${TIMEZONE}"
    echo "double_auth           : ${DOUBLEAUTH}"
    if ! ${YES}
    then
       logOkStep "Are these variables valid?  (y/N)"
       read
    fi
    if [[ $REPLY =~ ^[Yy]$ ]] || ${YES}
    then
       logOkStep "Continue script"
    else
       logOkStep "Change variables"
       change_variables
    fi

}

function set_database {
    logOkStep "set_database in data/database/db.env"
    sed -i "s|^\(POSTGRES_PASSWORD=\).*$|\1${POSTGRES_PASSWORD}|" ${DOCKERDIR}/data/database/db.env
    sed -i "s|^\(USER_NAME=\).*$|\1${USER_NAME}|" ${DOCKERDIR}/data/database/db.env
    sed -i "s|^\(USER_PASSWORD=\).*$|\1${USER_PASSWORD}|" ${DOCKERDIR}/data/database/db.env
    sed -i "s|^\(DB_PRODIGE_NAME=\).*$|\1${DB_PRODIGE_NAME}|" ${DOCKERDIR}/data/database/db.env
    sed -i "s|^\(DB_CATALOGUE_NAME=\).*$|\1${DB_CATALOGUE_NAME}|" ${DOCKERDIR}/data/database/db.env
    logOkStep "set_database in data/geonetwork/WEB-INF/config-overrides.properties"
    sed -i "s/\(overrides.jdbc.database=\).*$/\1${DB_CATALOGUE_NAME}/" ${DOCKERDIR}/data/geonetwork/WEB-INF/config-overrides.properties
    sed -i "s/\(overrides.jdbc.username=\).*$/\1${USER_NAME}/" ${DOCKERDIR}/data/geonetwork/WEB-INF/config-overrides.properties
    sed -i "s/\(overrides.jdbc.password=\).*$/\1${USER_PASSWORD}/" ${DOCKERDIR}/data/geonetwork/WEB-INF/config-overrides.properties
    logOkStep "set_database in data/*/conf/global_parameters.yaml"
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml ;
    do
        sed -i "s/^\([\ \t]*prodige_name:[\ \t]*\).*$/\1${DB_PRODIGE_NAME}/" $file
        sed -i "s/^\([\ \t]*catalogue_name:[\ \t]*\).*$/\1${DB_CATALOGUE_NAME}/" $file
        sed -i "s/^\([\ \t]*database_name:[\ \t]*\).*$/\1${DB_PRODIGE_NAME}/" $file
        sed -i "s/^\([\ \t]*prodige_user:[\ \t]*\).*$/\1${USER_NAME}/" $file
        sed -i "s/^\([\ \t]*catalogue_user:[\ \t]*\).*$/\1${USER_NAME}/" $file
        sed -i "s/^\([\ \t]*database_user:[\ \t]*\).*$/\1${USER_NAME}/" $file
        sed -i "s/^\([\ \t]*prodige_password:[\ \t]*\).*$/\1${USER_PASSWORD}/" $file
        sed -i "s/^\([\ \t]*catalogue_password:[\ \t]*\).*$/\1${USER_PASSWORD}/" $file
        sed -i "s/^\([\ \t]*database_password:[\ \t]*\).*$/\1${USER_PASSWORD}/" $file
    done
}

function set_ldap {
    logOkStep "set_ldap in data/openldap/db.env"
    sed -i "s|^\(LDAP_ADMIN_PASSWORD=\).*$|\1${LDAP_ADMIN_PASSWORD}|" ${DOCKERDIR}/data/openldap/db.env
    sed -i "s|^\(LDAP_CONFIG_PASSWORD=\).*$|\1${LDAP_CONFIG_PASSWORD}|" ${DOCKERDIR}/data/openldap/db.env
    logOkStep "set_ldap in data/geonetwork/WEB-INF/config-overrides.properties"
    sed -i "s/\(overrides.ldap.security.credentials=\).*$/\1${LDAP_ADMIN_PASSWORD}/" ${DOCKERDIR}/data/geonetwork/WEB-INF/config-overrides.properties
    logOkStep "set_ldap in data/*/conf/global_parameters.yaml"
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml ;
    do
        sed -i "s/^\([\ \t]*ldap_password:[\ \t]*\).*$/\1${LDAP_ADMIN_PASSWORD}/" $file
    done
    logOkStep "set_ldap admincli in data/*/conf/global_parameters.yaml"
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml ;
    do
        sed -i "s/^\([\ \t]*phpcli_default_password[\ \t]*:[\ \t]*\).*$/\1${LDAP_ADMINCLI_PASSWORD}/" $file
    done
    logOkStep "set_ldap admin in data/cas/conf/cas.properties"
    sed -i "s/\(cas\.authn\.ldap\[0\].bind-credential=\).*$/\1${LDAP_ADMIN_PASSWORD}/" ${DOCKERDIR}/data/cas/conf/cas.properties
}

function set_cas_key {
    logOkStep "set_cas_key in data/cas/conf/cas.properties"
    RANDOM_KEY=`openssl rand -hex 100 |head -c 43`
    sed -i "s/cas.tgc.crypto.encryption.key=\$/cas.tgc.crypto.encryption.key=${RANDOM_KEY}/" ${DOCKERDIR}/data/cas/conf/cas.properties
    RANDOM_KEY=`openssl rand -hex 100 |head -c 86`
    sed -i "s/cas.tgc.crypto.signing.key=\$/cas.tgc.crypto.signing.key=${RANDOM_KEY}/" ${DOCKERDIR}/data/cas/conf/cas.properties
    RANDOM_KEY=`openssl rand -hex 100 |head -c 22`
    sed -i "s/cas.webflow.crypto.encryption.key=\$/cas.webflow.crypto.encryption.key=${RANDOM_KEY}/" ${DOCKERDIR}/data/cas/conf/cas.properties
    RANDOM_KEY=`openssl rand -hex 100 |head -c 86`
    sed -i "s/cas.webflow.crypto.signing.key=\$/cas.webflow.crypto.signing.key=${RANDOM_KEY}/" ${DOCKERDIR}/data/cas/conf/cas.properties
}

function set_cas_double_auth {
    if $DOUBLEAUTH
    then
      logOkStep "set_cas_double_auth enable in data/cas/conf/cas.properties"
      sed -i 's/cas.authn.mfa.triggers.global.global-provider-id=.*/cas.authn.mfa.triggers.global.global-provider-id=mfa-simple/' ${DOCKERDIR}/data/cas/conf/cas.properties
      sed -i "s/cas.authn.mfa.simple.mail.from=.*/cas.authn.mfa.simple.mail.from=${DEBUG_MAIL}/" ${DOCKERDIR}/data/cas/conf/cas.properties
      sed -i "s/spring.mail.host=.*/spring.mail.host=${SMTP_RELAY}/" ${DOCKERDIR}/data/cas/conf/cas.properties
    else
      logOkStep "set_cas_double_auth disable in data/cas/conf/cas.properties"
      sed -i 's/cas.authn.mfa.triggers.global.global-provider-id=.*/cas.authn.mfa.triggers.global.global-provider-id=/' ${DOCKERDIR}/data/cas/conf/cas.properties
    fi
}

function set_smtp_relay {
    logOkStep "set_smtp_relay in data/conf/docker.env"
    if [ `grep "SMTP_RELAY" ${DOCKERDIR}/data/conf/docker.env` ]
    then
        echo "Fix SMTP_RELAY : ${SMTP_RELAY}"
        sed -i "s|^\(SMTP_RELAY=\).*$|\1${SMTP_RELAY}|" ${DOCKERDIR}/data/conf/docker.env
    else
        echo "Add SMTP_RELAY : ${SMTP_RELAY}"
        echo "SMTP_RELAY=${SMTP_RELAY}" >> ${DOCKERDIR}/data/conf/docker.env
    fi
    if [ -n "$SMTP_MYHOSTNAME" ]
    then
      if [ `grep "SMTP_MYHOSTNAME" ${DOCKERDIR}/data/conf/docker.env` ]
      then
          echo "Fix SMTP_MYHOSTNAME : ${SMTP_MYHOSTNAME}"
          sed -i "s|^\(SMTP_MYHOSTNAME=\).*$|\1${SMTP_MYHOSTNAME}|" ${DOCKERDIR}/data/conf/docker.env
      else
          echo "Add SMTP_MYHOSTNAME : ${SMTP_MYHOSTNAME}"
          echo "SMTP_MYHOSTNAME=${SMTP_MYHOSTNAME}" >> ${DOCKERDIR}/data/conf/docker.env
      fi
    fi
    if [ -n "$SMTP_MYHOSTNAME" ]
    then
      if [ `grep "SMTP_MYDOMAIN" ${DOCKERDIR}/data/conf/docker.env` ]
      then
          echo "Fix SMTP_MYDOMAIN : ${SMTP_MYDOMAIN}"
          sed -i "s|^\(SMTP_MYDOMAIN=\).*$|\1${SMTP_MYDOMAIN}|" ${DOCKERDIR}/data/conf/docker.env
      else
          echo "Add SMTP_MYDOMAIN : ${SMTP_MYDOMAIN}"
          echo "SMTP_MYDOMAIN=${SMTP_MYDOMAIN}" >> ${DOCKERDIR}/data/conf/docker.env
      fi
    fi
}
function set_debug_mail {
    logOkStep "set_debug_mail in data/*/conf/global_parameters.yaml"
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml ;
    do
        sed -i "s/^\([\ \t]*app_mail:[\ \t]*\).*$/\1${DEBUG_MAIL}/" $file
        sed -i "s/^\([\ \t]*debug_mail:[\ \t]*\).*$/\1${DEBUG_MAIL}/" $file
    done
}

function set_timezone {
    logOkStep "set_timezone in data/conf/docker.env"
    sed -i "s|^\(TZ=\).*$|\1${TIMEZONE}|" ${DOCKERDIR}/data/conf/docker.env
    logOkStep "set_timezone in data/conf/tomcat_timezone"
    echo "${TIMEZONE}" > ${DOCKERDIR}/data/conf/tomcat_timezone
    logOkStep "set_timezone in data/database/postgresql.conf"
    sed -i "s|^\(timezone =\).*$|\1 \'${TIMEZONE}\'|" ${DOCKERDIR}/data/database/postgresql.conf
}

# NB : voir aussi le mot de passe de tampon !
# find . -name "global_parameters.yaml" -exec sed -i 's/^\([\ \t]*tampon_password:[\ \t]*\).*$/\1password/' {} +

function set_parameters {
    set_database
    set_ldap
    set_cas_key
    set_cas_double_auth
    set_smtp_relay
    set_debug_mail
    set_timezone
}
########################################################################################################################################

##################################
# main                           #
##################################

MODE="main"
logOk "############################"
logOk "########### main ###########"
logOk "############################"
# Options
OPTS=$( getopt -o h,u:,w:,z:,g:,l:,x:,f:,i:,j:,o:,b:,A,y -l help,user:,password:,postgres-password:,db-prodige-name:,db-catalogue-name:,ldap-admin-password:,ldap-config-password:,ldap-admincli-password:,smtp-relay:,smtp-myhostname:,smtp-mydomain:,debug-mail:,timezone:,double-auth,yes -- "$@" )
ifCmdFail "Option not found, use \"$SCRIPT -h\""
eval set -- "$OPTS"
while true
do
    case "$1" in
        -h | --help)
        usage
        exit 0
        ;;
        -u | --user)
        USER_NAME=$2
        shift 2
        ;;
        -w | --password)
        USER_PASSWORD=$2
        shift 2
        ;;
        -z | --postgres-password)
        POSTGRES_PASSWORD=$2
        shift 2
        ;;
        -g | --db-prodige-name)
        DB_PRODIGE_NAME=$2
        shift 2
        ;;
        -l | --db-catalogue-name)
        DB_CATALOGUE_NAME=$2
        shift 2
        ;;
        -x | --ldap-admin-password)
        LDAP_ADMIN_PASSWORD=$2
        shift 2
        ;;
        -f | --ldap-config-password)
        LDAP_CONFIG_PASSWORD=$2
        shift 2
        ;;
        -i | --ldap-admincli-password)
        LDAP_ADMINCLI_PASSWORD=$2
        shift 2
        ;;
        -j | --smtp-relay)
        SMTP_RELAY=$2
        shift 2
        ;;
        --smtp-myhostname)
        SMTP_MYHOSTNAME=$2
        shift 2
        ;;
        --smtp-mydomain)
        SMTP_MYDOMAIN=$2
        shift 2
        ;;
        -b | --debug-mail)
        DEBUG_MAIL=$2
        shift 2
        ;;
        -o | --timezone)
        TIMEZONE=$2
        shift 2
        ;;
        -A | --double-auth)
        DOUBLEAUTH=true
        shift
        ;;
        -y | --yes)
        YES=true
        shift
        ;;
        --)
        shift
        break
        ;;
    esac
done

if [[ $(id -u) -ne 0 ]] ; then
    logError "You must be root or sudo to execute $SCRIPTNAME"
    exit 1
fi

# Step

NB_STEP_AL=2

STEP_AL=1
MODE="Validation"
logOkStep "Validation of variables"
validation

STEP_AL=2
MODE="SetParameters"
logOkStep "Set parameters"
set_parameters

logOk "End of ${SCRIPTNAME}"
# End
