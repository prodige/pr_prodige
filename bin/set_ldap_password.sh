#!/bin/bash

SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

DOCKERDIR=${SCRIPTDIR}/..

# Date
DATE="`date '+%Y%m%d'`"

####################################################################################################################

##################################
# Usage                          #
##################################

function usage {
    logOk "Usage"
    printf "\t$SCRIPTNAME [arg...]\n"
    printf "Options :\n"
    printf "\t$SCRIPTNAME -h | --help                                       : Help\n"
}

########################################################################################################################################

##################################
# Print log                      #
##################################
# Bash color
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m'
MODE="init"
function logOk {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logError {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarning {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function logOkStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logErrorStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarningStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function ifCmdFailStep {
    if [ $? -ne 0 ]; then
        logErrorStep $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}

function ifCmdFail {
    if [ $? -ne 0 ]; then
        logError $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}


########################################################################################################################################


##################################
# Function                       #
##################################


function get_ldap_parameters {
    PARAM_FILE="${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml"
    LDAP_BASE_DN=$(eval echo $(awk -F":" '/ldap_base_dn/ { print $2 }' ${PARAM_FILE}))
    LDAP_SEARCH_DN=$(eval echo $(awk -F":" '/ldap_search_dn/ { print $2 }' ${PARAM_FILE}))
    LDAP_PASSWORD=$(eval echo $(awk -F":" '/ldap_password/ { print $2 }' ${PARAM_FILE}))
    PHPCLI_DEFAULT_LOGIN=$(eval echo $(awk -F":" '/phpcli_default_login/ { print $2 }' ${PARAM_FILE}))
    PHPCLI_DEFAULT_PASSWORD=$(eval echo $(awk -F":" '/phpcli_default_password/ { print $2 }' ${PARAM_FILE}))
}

function set_ldap {
    logOkStep "Set ldap ${PHPCLI_DEFAULT_LOGIN} password"
    docker exec -t jpr_prodige_cas_ldap ldappasswd -v -D ${LDAP_SEARCH_DN} -w ${LDAP_PASSWORD} -s ${PHPCLI_DEFAULT_PASSWORD} "uid=${PHPCLI_DEFAULT_LOGIN},ou=Users,${LDAP_BASE_DN}"
    ifCmdFailStep "Change ldap password failed"
}

function check_ldap {
    logOkStep "Check ldap ${PHPCLI_DEFAULT_LOGIN} password"
    docker exec -ti jpr_prodige_cas_ldap ldapsearch -b "uid=${PHPCLI_DEFAULT_LOGIN},ou=Users,${LDAP_BASE_DN}" -D ${LDAP_SEARCH_DN} -w ${LDAP_PASSWORD} >/dev/null
    ifCmdFailStep "Check ldap passord failed"
}

########################################################################################################################################

##################################
# main                           #
##################################

MODE="main"
logOk "############################"
logOk "########### main ###########"
logOk "############################"
# Options
OPTS=$( getopt -o h -l help -- "$@" )
ifCmdFail "Option not found, use \"$SCRIPT -h\""
eval set -- "$OPTS"
while true
do
    case "$1" in
        -h | --help)
        usage
        exit 0
        ;;
        --)
        shift
        break
        ;;
    esac
done

# Step

NB_STEP_AL=3

STEP_AL=1
MODE="GetLdapParameters"
logOkStep "Get ldap parameters"
get_ldap_parameters

STEP_AL=2
MODE="SetLdapValues"
logOkStep "Set ldap values"
set_ldap

STEP_AL=3
MODE="CheckLdapValues"
logOkStep "Check ldap values"
check_ldap

logOk "End of ${SCRIPTNAME}"
# End
