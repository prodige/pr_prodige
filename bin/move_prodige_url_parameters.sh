#!/bin/bash

SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

DOCKERDIR="${SCRIPTDIR}/.."

YES=false

# Date
DATE="`date '+%Y%m%d'`"

# URL
DOMAIN=".prodige.internal"
ADMINCARTO="admincarto"
ADMINSITE="adminsite"
CARTO="carto"
CAS="cas"
CATALOGUE="catalogue"
DATACARTO="datacarto"
MAPSERV="mapserv"
PRINT="print"
TABLE="table"
TELECARTO="telecarto"

####################################################################################################################

##################################
# Usage                          #
##################################

function usage {
    logOk "Usage"
    printf "\t$SCRIPTNAME [arg...]\n"
    printf "Options :\n"
    printf "\t$SCRIPTNAME -h | --help                                       : Help\n"
    printf "\t$SCRIPTNAME -d <domain> | --domain                            : Domain of prodige. default: .prodige.internal (ex: .alkante.com or -test.alkante.com) \n"
    printf "\t$SCRIPTNAME -a <admincarto> | --admincarto                    : Subdomain of admincarto (default: admincarto)\n"
    printf "\t$SCRIPTNAME -s <adminsite> | --adminsite                      : Subdomain of adminsite (default: adminsite)\n"
    printf "\t$SCRIPTNAME -r <carto> | --carto                              : Subdomain of carto (default: carto)\n"
    printf "\t$SCRIPTNAME -k <cas> | --cas                                  : Subdomain of cas (default: cas)\n"
    printf "\t$SCRIPTNAME -c <catalogue> | --catalogue                      : Subdomain of catalogue (default: catalogue)\n"
    printf "\t$SCRIPTNAME -q <datacarto> | --datacarto                      : Subdomain of datacarto (default: datacarto)\n"
    printf "\t$SCRIPTNAME -m <mapserv> | --mapserv                          : Subdomain of mapserv (default: mapserv)\n"
    printf "\t$SCRIPTNAME -p <print> | --print                              : Subdomain of print (default: print)\n"
    printf "\t$SCRIPTNAME -t <table> | --table                              : Subdomain of table (default: table)\n"
    printf "\t$SCRIPTNAME -e <telecarto> | --telecarto                      : Subdomain of telecarto (default: telecarto)\n"
    printf "\t$SCRIPTNAME -y | --yes                                        : Non-interactive mode\n"
}

########################################################################################################################################

##################################
# Print log                      #
##################################
# Bash color
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m'
MODE="init"
function logOk {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logError {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarning {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function logOkStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logErrorStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarningStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function ifCmdFailStep {
    if [ $? -ne 0 ]; then
        logErrorStep $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}

function ifCmdFail {
    if [ $? -ne 0 ]; then
        logError $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}


########################################################################################################################################


##################################
# Function                       #
##################################

function change_variables {
    logOkStep "If no change, press enter"
    printf "Set domain : ${DOMAIN} -> "
    read
    if [[ $REPLY != '' ]]
    then
       DOMAIN="$REPLY"
    fi
    printf "Set admincarto : ${ADMINCARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       ADMINCARTO="$REPLY"
    fi
    printf "Set adminsite : ${ADMINSITE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       ADMINSITE="$REPLY"
    fi
    printf "Set carto : ${CARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       CARTO="$REPLY"
    fi
    printf "Set cas : ${CAS} -> "
    read
    if [[ $REPLY != '' ]]
    then
       CAS="$REPLY"
    fi
    printf "Set catalogue : ${CATALOGUE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       CATALOGUE="$REPLY"
    fi
    printf "Set datacarto : ${DATACARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       DATACARTO="$REPLY"
    fi
    printf "Set mapserv : ${MAPSERV} -> "
    read
    if [[ $REPLY != '' ]]
    then
       MAPSERV="$REPLY"
    fi
    printf "Set print : ${PRINT} -> "
    read
    if [[ $REPLY != '' ]]
    then
       PRINT="$REPLY"
    fi
    printf "Set table : ${TABLE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       TABLE="$REPLY"
    fi
    printf "Set telecarto : ${TELECARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       TELECARTO="$REPLY"
    fi
    validation
}

function validation {
    logOkStep "Validation : "
    echo "domain    : ${DOMAIN}"
    echo "subdomain :"
    echo "    admincarto : ${ADMINCARTO}${DOMAIN}"
    echo "    adminsite  : ${ADMINSITE}${DOMAIN}"
    echo "    carto      : ${CARTO}${DOMAIN}"
    echo "    cas        : ${CAS}${DOMAIN}"
    echo "    catalogue  : ${CATALOGUE}${DOMAIN}"
    echo "    datacarto  : ${DATACARTO}${DOMAIN}"
    echo "    mapserv    : ${MAPSERV}${DOMAIN}"
    echo "    print      : ${PRINT}${DOMAIN}"
    echo "    table      : ${TABLE}${DOMAIN}"
    echo "    telecarto  : ${TELECARTO}${DOMAIN}"
    if ! ${YES}
    then
       logOkStep "Are these variables valid?  (y/n)"
       read
    fi
    if [[ $REPLY =~ ^[Yy]$ ]] || ${YES}
    then
       logOkStep "Continue script"
    else
       logOkStep "Change variables"
       change_variables
    fi

}

function set_admincarto {
    logOkStep "set_admincarto"
    sed -i "s|^\([\ \t]*phppgadmin_url:[\ \t]*\).*$|\1'https://${ADMINCARTO}${DOMAIN}/phpPgAdmin'|" ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml \
        ;
    do
        sed -i "s|^\([\ \t]*PRODIGE_URL_ADMINCARTO[\ \t]*:[\ \t]*\).*$|\1\"https://${ADMINCARTO}${DOMAIN}\"|" $file
    done
}

function set_adminsite {
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml \
        ;
    do
        sed -i "s|^\([\ \t]*PRODIGE_URL_ADMINSITE[\ \t]*:[\ \t]*\).*$|\1\"https://${ADMINSITE}${DOMAIN}\"|" $file
    done
}

function set_carto {
    sed -i "s|^\([\ \t]*\$ENV.serverUrl[\ \t]*=[\ \t]*\).*$|\1\'https://${CARTO}${DOMAIN}/';|" ${DOCKERDIR}/data/visualiseur/conf/env.js
    sed -i "s|^\([\ \t]*window.__env.serverUrl[\ \t]*=[\ \t]*\).*$|\1\'https://${CARTO}${DOMAIN}/';|" ${DOCKERDIR}/data/visualiseur-core-print/conf/env.js
    sed -i "s|^\([\ \t]*window.__env.frontCartoUrl[\ \t]*=[\ \t]*\).*$|\1\'https://${CARTO}${DOMAIN}/';|" ${DOCKERDIR}/data/table/conf/env.js
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml \
        ;
    do
        sed -i "s|^\([\ \t]*PRODIGE_URL_FRONTCARTO[\ \t]*:[\ \t]*\).*$|\1\"https://${CARTO}${DOMAIN}\"|" $file
        sed -i "s|^\([\ \t]*PRODIGE_URL_FRONTVIEWER[\ \t]*:[\ \t]*\).*$|\1\"https://${CARTO}${DOMAIN}\"|" $file
    done
}

function set_cas {
    sed -i "s|^\(cas.server.name=\).*$|\1https://${CAS}${DOMAIN}|" ${DOCKERDIR}/data/cas/conf/cas.properties
    sed -i "s|^\(overrides.cas.baseURL=\).*$|\1https://${CAS}${DOMAIN}|" ${DOCKERDIR}/data/geonetwork/WEB-INF/config-overrides.properties
    sed -i "s|^\(overrides.cas.ticket.validator.url=\).*$|\1https://${CAS}${DOMAIN}|" ${DOCKERDIR}/data/geonetwork/WEB-INF/config-overrides.properties
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml \
        ;
    do
        sed -i "s|^\([\ \t]*cas_host:[\ \t]*\).*$|\1'${CAS}${DOMAIN}'|" $file
    done
}

function set_catalogue {
    sed -i "s|^\(prodige.forgotpwd.url=\).*$|\1https://${CATALOGUE}${DOMAIN}/admin/chgpwd|" ${DOCKERDIR}/data/cas/conf/cas.properties
    sed -i "s|^\(password.policy.url=\).*$|\1https://${CATALOGUE}${DOMAIN}/admin/chgpwd|" ${DOCKERDIR}/data/cas/conf/cas.properties
    sed -i "s|^\(prodige.checkaccount.url=\).*$|\1https://${CATALOGUE}${DOMAIN}/admin/chgpwd/checkaccount/{userId}|" ${DOCKERDIR}/data/cas/conf/cas.properties
    sed -i "s|^\([\ \t]*var[\ \t]*baseUrl[\ \t]*=[\ \t]*\).*$|\1\'https://${CATALOGUE}${DOMAIN}/admin/chgpwd';|g" ${DOCKERDIR}/data/cas/conf/managepwd.js
    sed -i "s|^\(overrides.geonetwork.https.url=\).*$|\1https://${CATALOGUE}${DOMAIN}/geonetwork|" ${DOCKERDIR}/data/geonetwork/WEB-INF/config-overrides.properties
    sed -i "s|^\([\ \t]*window.__env.catalogueUrl[\ \t]*=[\ \t]*\).*$|\1\'https://${CATALOGUE}${DOMAIN}';|" ${DOCKERDIR}/data/table/conf/env.js
    sed -i "s|^\([\ \t]*window.__env.geonetworkUrl[\ \t]*=[\ \t]*\).*$|\1\'https://${CATALOGUE}${DOMAIN}/geonetwork';|" ${DOCKERDIR}/data/table/conf/env.js
    sed -i "s|^\([\ \t]*catalogue_url:[\ \t]*\).*$|\1'https://${CATALOGUE}${DOMAIN}'|" ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml \
        ;
    do
        sed -i "s|^\([\ \t]*PRODIGE_URL_CATALOGUE[\ \t]*:[\ \t]*\).*$|\1\"https://${CATALOGUE}${DOMAIN}\"|" $file
    done
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml \
        ;
    do
        sed -i "s|^\([\ \t]*PRO_GEONETWORK_URLBASE[\ \t]*:[\ \t]*\).*$|\1\"https://${CATALOGUE}${DOMAIN}/geonetwork/\"|" $file
    done

}

function set_datacarto {
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml \
        ;
    do
        sed -i "s|^\([\ \t]*PRODIGE_URL_DATACARTO[\ \t]*:[\ \t]*\).*$|\1\"https://${DATACARTO}${DOMAIN}\"|" $file
    done
}

function set_mapserv {
    sed -i "s|^\([\ \t]*api_url:[\ \t]*\).*$|\1'https://${MAPSERV}${DOMAIN}'|" ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml \
        ;
    do
        sed -i "s|^\([\ \t]*PRODIGE_URL_ADMINMAPSERV[\ \t]*:[\ \t]*\).*$|\1\"https://${MAPSERV}${DOMAIN}\"|" $file
    done
}

function set_print {
    for file in \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml ;
    do
        sed -i "s|^\([\ \t]*PRODIGE_URL_FRONTCARTO_PRINTER[\ \t]*:[\ \t]*\).*$|\1\"https://${PRINT}${DOMAIN}\"|" $file
    done
}

function set_table {
    for file in \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml ;
    do
        sed -i "s|^\([\ \t]*PRODIGE_URL_TABLEVIEWER[\ \t]*:[\ \t]*\).*$|\1\"https://${TABLE}${DOMAIN}\"|" $file
    done
}

function set_telecarto {
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml \
        ;
    do
        sed -i "s|^\([\ \t]*PRODIGE_URL_TELECARTO[\ \t]*:[\ \t]*\).*$|\1\"https://${TELECARTO}${DOMAIN}\"|" $file
    done
}

function set_domain {
    # leading char is separator - "trailing domain" is what is after the first char
    SEPARATOR=${DOMAIN:0:1}
    TRAILINGDOMAIN=${DOMAIN:1:${#DOMAIN}}
    sed -i "s|^\([\ \t]*dns_url_prefix_sep:[\ \t]*\).*$|\1'$SEPARATOR'|" ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml
    sed -i "s|^\([\ \t]*domain:[\ \t]*\).*$|\1\"$TRAILINGDOMAIN\"|" ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml
}

function set_cas_proxy_chain {
    for file in \
        ${DOCKERDIR}/data/admincarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/adminsite/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/datacarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/mapserver/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/telecarto/conf/global_parameters.yaml \
        ${DOCKERDIR}/data/visualiseur-back/conf/global_parameters.yaml ;
    do
        sed -i "s%^\([\ \t]*cas_proxy_chain:[\ \t]*\).*$%\1[ \"/^https?:\\\\\\\\/\\\\\\\\/(${CATALOGUE}|${ADMINCARTO}|${ADMINSITE}|${TELECARTO}|${CARTO}|${PRINT})${DOMAIN}\\\\\\\\/.*/\" ]%" $file
        # result shall look like cas_proxy_chain: [ "/^https?:\\/\\/(geoportail|catalogue|admincarto|adminsite|telecarto|carto|print).nexafrika.com\\/.*/" ]
    done
    # Similar to cas_proxy_chain
    sed -i "s%^\([\ \t]*\"serviceId\"[\ \t]*:[\ \t]*\).*$%\1\"^https?://${ADMINCARTO}${DOMAIN}/.*\",%" ${DOCKERDIR}/data/cas/services/prodigeproxy-admincarto-15003.json
    sed -i "s%^\([\ \t]*\"logoutUrl\"[\ \t]*:[\ \t]*\).*$%\1\"https://${ADMINCARTO}${DOMAIN}\",%" ${DOCKERDIR}/data/cas/services/prodigeproxy-admincarto-15003.json
    sed -i "s%^\([\ \t]*\"serviceId\"[\ \t]*:[\ \t]*\).*$%\1\"^https?://${ADMINSITE}${DOMAIN}/.*\",%" ${DOCKERDIR}/data/cas/services/prodigeproxy-adminsite-15002.json
    sed -i "s%^\([\ \t]*\"logoutUrl\"[\ \t]*:[\ \t]*\).*$%\1\"https://${ADMINSITE}${DOMAIN}\",%" ${DOCKERDIR}/data/cas/services/prodigeproxy-adminsite-15002.json
    sed -i "s%^\([\ \t]*\"serviceId\"[\ \t]*:[\ \t]*\).*$%\1\"^https?://${CARTO}${DOMAIN}/.*\",%" ${DOCKERDIR}/data/cas/services/prodigeproxy-carto-15010.json
    sed -i "s%^\([\ \t]*\"logoutUrl\"[\ \t]*:[\ \t]*\).*$%\1\"https://${CARTO}${DOMAIN}/carto/config/checkuser\",%" ${DOCKERDIR}/data/cas/services/prodigeproxy-carto-15010.json
    sed -i "s%^\([\ \t]*\"serviceId\"[\ \t]*:[\ \t]*\).*$%\1\"^https?://${CATALOGUE}${DOMAIN}/.*\",%" ${DOCKERDIR}/data/cas/services/prodigeproxy-catalogue-15005.json
    sed -i "s%^\([\ \t]*\"logoutUrl\"[\ \t]*:[\ \t]*\).*$%\1\"https://${CATALOGUE}${DOMAIN}/prodige/connect\",%" ${DOCKERDIR}/data/cas/services/prodigeproxy-catalogue-15005.json
    sed -i "s%^\([\ \t]*\"serviceId\"[\ \t]*:[\ \t]*\).*$%\1\"^https?://${TELECARTO}${DOMAIN}/.*\",%" ${DOCKERDIR}/data/cas/services/prodigeproxy-telecarto-15006.json
    sed -i "s%^\([\ \t]*\"logoutUrl\"[\ \t]*:[\ \t]*\).*$%\1\"https://${TELECARTO}${DOMAIN}\",%" ${DOCKERDIR}/data/cas/services/prodigeproxy-telecarto-15006.json
}

function set_config-overrides-prod {
    sed -i '/<prodige>/,/<\/prodige>/c\
      <prodige>\
        <catalogue>\
          <url>https://'"${CATALOGUE}${DOMAIN}"'</url>\
        </catalogue>\
        <broadcast-connect>\
          <url>https://'"${ADMINCARTO}${DOMAIN}"'</url>\
          <url>https://'"${CARTO}${DOMAIN}"'</url>\
          <url>https://'"${TELECARTO}${DOMAIN}"'</url>\
        </broadcast-connect>\
      </prodige>' ${DOCKERDIR}/data/geonetwork/WEB-INF/config-overrides-prod.xml
}

function set_parameters {
    set_admincarto
    set_adminsite
    set_carto
    set_cas
    set_catalogue
    set_datacarto
    set_mapserv
    set_print
    set_table
    set_telecarto
    # separator / domain
    # used in https://gitlab.alkante.com/alkante/ppr_prodige_admincarto/blob/master/site/src/Migrations/Version20191130040102.php
    set_domain
    # variables with several parameters
    set_cas_proxy_chain
    set_config-overrides-prod
}
########################################################################################################################################

##################################
# main                           #
##################################

MODE="main"
logOk "############################"
logOk "########### main ###########"
logOk "############################"
# Options
OPTS=$( getopt -o h,d:,a:,s:,r:,k:,c:,q:,m:,p:,t:,e:,y -l help,domain:,admincarto:,adminsite:,carto:,cas:,catalogue:,datacarto:,mapserv:,print:,table:,telecarto:,yes -- "$@" )
ifCmdFail "Option not found, use \"$SCRIPT -h\""
eval set -- "$OPTS"
while true
do
    case "$1" in
        -h | --help)
        usage
        exit 0
        ;;
        -d | --domain)
        DOMAIN=$2
        shift 2
        ;;
        -a | --admincarto)
        ADMINCARTO=$2
        shift 2
        ;;
        -s | --adminsite)
        ADMINSITE=$2
        shift 2
        ;;
        -r | --carto)
        CARTO=$2
        shift 2
        ;;
        -k | --cas)
        CAS=$2
        shift 2
        ;;
        -c | --catalogue)
        CATALOGUE=$2
        shift 2
        ;;
        -q | --datacarto)
        DATACARTO=$2
        shift 2
        ;;
        -m | --mapserv)
        MAPSERV=$2
        shift 2
        ;;
        -p | --print)
        PRINT=$2
        shift 2
        ;;
        -t | --table)
        TABLE=$2
        shift 2
        ;;
        -e | --telecarto)
        TELECARTO=$2
        shift 2
        ;;
        -y | --yes)
        YES=true
        shift
        ;;
        --)
        shift
        break
        ;;
    esac
done

if [[ $(id -u) -ne 0 ]] ; then
    logError "You must be root or sudo to execute $SCRIPTNAME"
    exit 1
fi

# Step

NB_STEP_AL=2

STEP_AL=1
MODE="Validation"
logOkStep "Validation of variables"
validation

STEP_AL=2
MODE="SetParameters"
logOkStep "Set parameters"
set_parameters

logOk "End of ${SCRIPTNAME}"
# End
