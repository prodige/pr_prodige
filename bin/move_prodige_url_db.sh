#!/bin/bash

SCRIPT=`readlink -f $0`
SCRIPTDIR=`dirname $SCRIPT`
SCRIPTNAME=`basename $SCRIPT`

DOCKERDIR=${SCRIPTDIR}/..

YES=false

# Date
DATE="`date '+%Y%m%d'`"

# URL
DOMAIN=".prodige.internal"
ADMINCARTO="admincarto"
ADMINSITE="adminsite"
CARTO="carto"
CAS="cas"
CATALOGUE="catalogue"
DATACARTO="datacarto"
MAPSERV="mapserv"
PRINT="print"
TABLE="table"
TELECARTO="telecarto"
# OLD URL
OLDDOMAIN=".prodige.internal"
OLDADMINCARTO="admincarto"
OLDADMINSITE="adminsite"
OLDCARTO="carto"
OLDCAS="cas"
OLDCATALOGUE="catalogue"
OLDDATACARTO="datacarto"
OLDMAPSERV="mapserv"
OLDPRINT="print"
OLDTABLE="table"
OLDTELECARTO="telecarto"

DISABLELDAP=false

####################################################################################################################

##################################
# Usage                          #
##################################

function usage {
    logOk "Usage"
    printf "\t$SCRIPTNAME [arg...]\n"
    printf "Options :\n"
    printf "\t$SCRIPTNAME -h | --help                                       : Help\n"
    printf "\t$SCRIPTNAME -d <domain> | --domain                            : Domain of prodige. default: .prodige.internal (ex: .alkante.com or -test.alkante.com) \n"
    printf "\t$SCRIPTNAME -a <admincarto> | --admincarto                    : Subdomain of admincarto (default: admincarto)\n"
    printf "\t$SCRIPTNAME -s <adminsite> | --adminsite                      : Subdomain of adminsite (default: adminsite)\n"
    printf "\t$SCRIPTNAME -r <carto> | --carto                              : Subdomain of carto (default: carto)\n"
    printf "\t$SCRIPTNAME -k <cas> | --cas                                  : Subdomain of cas (default: cas)\n"
    printf "\t$SCRIPTNAME -c <catalogue> | --catalogue                      : Subdomain of catalogue (default: catalogue)\n"
    printf "\t$SCRIPTNAME -q <datacarto> | --datacarto                      : Subdomain of datacarto (default: datacarto)\n"
    printf "\t$SCRIPTNAME -m <mapserv> | --mapserv                          : Subdomain of mapserv (default: mapserv)\n"
    printf "\t$SCRIPTNAME -p <print> | --print                              : Subdomain of print (default: print)\n"
    printf "\t$SCRIPTNAME -t <table> | --table                              : Subdomain of table (default: table)\n"
    printf "\t$SCRIPTNAME -e <telecarto> | --telecarto                      : Subdomain of telecarto (default: telecarto)\n"
    printf "\t$SCRIPTNAME -y | --yes                                        : Non-interactive mode\n"
    printf "\t$SCRIPTNAME -D <olddomain> | --olddomain                      : Old domain of prodige. default: .prodige.internal (ex: .alkante.com or -test.alkante.com) \n"
    printf "\t$SCRIPTNAME -A <oldadmincarto> | --oldadmincarto              : Old subdomain of admincarto (default: admincarto)\n"
    printf "\t$SCRIPTNAME -S <oldadminsite> | --oldadminsite                : Old subdomain of adminsite (default: adminsite)\n"
    printf "\t$SCRIPTNAME -R <oldcarto> | --oldcarto                        : Old subdomain of carto (default: carto)\n"
    printf "\t$SCRIPTNAME -K <oldcas> | --oldcas                            : Old subdomain of cas (default: cas)\n"
    printf "\t$SCRIPTNAME -C <oldcatalogue> | --oldcatalogue                : Old subdomain of catalogue (default: catalogue)\n"
    printf "\t$SCRIPTNAME -Q <olddatacarto> | --olddatacarto                : Old subdomain of datacarto (default: datacarto)\n"
    printf "\t$SCRIPTNAME -M <oldmapserv> | --oldmapserv                    : Old subdomain of mapserv (default: mapserv)\n"
    printf "\t$SCRIPTNAME -P <oldprint> | --oldprint                        : Old subdomain of print (default: print)\n"
    printf "\t$SCRIPTNAME -T <oldtable> | --oldtable                        : Old subdomain of table (default: table)\n"
    printf "\t$SCRIPTNAME -E <oldtelecarto> | --oldtelecarto                : Old subdomain of telecarto (default: telecarto)\n"
    printf "\t$SCRIPTNAME --disableldap                                     : No update of ldap admincli password in database\n"
}

########################################################################################################################################

##################################
# Print log                      #
##################################
# Bash color
GREEN='\033[0;32m'
RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m'
MODE="init"
function logOk {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logError {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarning {
    echo -e "$SCRIPTNAME:$MODE:`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function logOkStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${GREEN}$@${NC}"
}

function logErrorStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${RED}ERROR : $@${NC}"
}

function logWarningStep {
    echo -e "$SCRIPTNAME:$MODE($STEP_AL/$NB_STEP_AL):`date --iso-8601=seconds`> ${YELLOW}WARNING : $@${NC}"
}

function ifCmdFailStep {
    if [ $? -ne 0 ]; then
        logErrorStep $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}

function ifCmdFail {
    if [ $? -ne 0 ]; then
        logError $1
        if [ "$SEND_MAIL" = true ]
        then
            NEED_MAIL=true
            sendMailIfNeeded
        fi
        exit 1
    fi
}


########################################################################################################################################


##################################
# Function                       #
##################################

function change_variables {
    logOkStep "If no change, press enter"
    printf "Set domain : ${DOMAIN} -> "
    read
    if [[ $REPLY != '' ]]
    then
       DOMAIN="$REPLY"
    fi
    printf "Set admincarto : ${ADMINCARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       ADMINCARTO="$REPLY"
    fi
    printf "Set adminsite : ${ADMINSITE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       ADMINSITE="$REPLY"
    fi
    printf "Set carto : ${CARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       CARTO="$REPLY"
    fi
    printf "Set cas : ${CAS} -> "
    read
    if [[ $REPLY != '' ]]
    then
       CAS="$REPLY"
    fi
    printf "Set catalogue : ${CATALOGUE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       CATALOGUE="$REPLY"
    fi
    printf "Set datacarto : ${DATACARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       DATACARTO="$REPLY"
    fi
    printf "Set mapserv : ${MAPSERV} -> "
    read
    if [[ $REPLY != '' ]]
    then
       MAPSERV="$REPLY"
    fi
    printf "Set print : ${PRINT} -> "
    read
    if [[ $REPLY != '' ]]
    then
       PRINT="$REPLY"
    fi
    printf "Set table : ${TABLE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       TABLE="$REPLY"
    fi
    printf "Set telecarto : ${TELECARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       TELECARTO="$REPLY"
    fi
    # OLD variables
    printf "Set old domain : ${OLDDOMAIN} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDDOMAIN="$REPLY"
    fi
    printf "Set old admincarto : ${OLDADMINCARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDADMINCARTO="$REPLY"
    fi
    printf "Set old adminsite : ${OLDADMINSITE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDADMINSITE="$REPLY"
    fi
    printf "Set old carto : ${OLDCARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDCARTO="$REPLY"
    fi
    printf "Set old cas : ${OLDCAS} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDCAS="$REPLY"
    fi
    printf "Set old catalogue : ${OLDCATALOGUE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDCATALOGUE="$REPLY"
    fi
    printf "Set old datacarto : ${OLDDATACARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDDATACARTO="$REPLY"
    fi
    printf "Set old mapserv : ${OLDMAPSERV} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDMAPSERV="$REPLY"
    fi
    printf "Set old print : ${OLDPRINT} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDPRINT="$REPLY"
    fi
    printf "Set old table : ${OLDTABLE} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDTABLE="$REPLY"
    fi
    printf "Set old telecarto : ${OLDTELECARTO} -> "
    read
    if [[ $REPLY != '' ]]
    then
       OLDTELECARTO="$REPLY"
    fi
    validation
}

function validation {
    logOkStep "Validation : "
    echo "domain    : ${DOMAIN}"
    echo "subdomain :"
    echo "    admincarto : ${ADMINCARTO}${DOMAIN}"
    echo "    adminsite  : ${ADMINSITE}${DOMAIN}"
    echo "    carto      : ${CARTO}${DOMAIN}"
    echo "    cas        : ${CAS}${DOMAIN}"
    echo "    catalogue  : ${CATALOGUE}${DOMAIN}"
    echo "    datacarto  : ${DATACARTO}${DOMAIN}"
    echo "    mapserv    : ${MAPSERV}${DOMAIN}"
    echo "    print      : ${PRINT}${DOMAIN}"
    echo "    table      : ${TABLE}${DOMAIN}"
    echo "    telecarto  : ${TELECARTO}${DOMAIN}"
    echo "old domain    : ${OLDDOMAIN}"
    echo "old subdomain :"
    echo "    old admincarto : ${OLDADMINCARTO}${OLDDOMAIN}"
    echo "    old adminsite  : ${OLDADMINSITE}${OLDDOMAIN}"
    echo "    old carto      : ${OLDCARTO}${OLDDOMAIN}"
    echo "    old cas        : ${OLDCAS}${OLDDOMAIN}"
    echo "    old catalogue  : ${OLDCATALOGUE}${OLDDOMAIN}"
    echo "    old datacarto  : ${OLDDATACARTO}${OLDDOMAIN}"
    echo "    old mapserv    : ${OLDMAPSERV}${OLDDOMAIN}"
    echo "    old print      : ${OLDPRINT}${OLDDOMAIN}"
    echo "    old table      : ${OLDTABLE}${OLDDOMAIN}"
    echo "    old telecarto  : ${OLDTELECARTO}${OLDDOMAIN}"
    if ! ${YES}
    then
       logOkStep "Are these variables valid?  (y/n)"
       read
    fi
    if [[ $REPLY =~ ^[Yy]$ ]] || ${YES}
    then
       logOkStep "Continue script"
    else
       logOkStep "Change variables"
       change_variables
    fi

}

function get_db_parameters {
    PARAM_FILE="${DOCKERDIR}/data/catalogue/conf/global_parameters.yaml"
    user_bdd=$(eval echo $(awk '/catalogue_user/ { print $2 }' ${PARAM_FILE}))
    pass_bdd=$(eval echo $(awk '/catalogue_password/ { print $2 }' ${PARAM_FILE}))
    # sur prodige CATALOGUE="CATALOGUE" / puis CATALOGUE=alk_respire
    CATALOGUE_DB=$(eval echo $(awk '/catalogue_name/ { print $2 }' ${PARAM_FILE}))
    PRODIGE_DB=$(eval echo $(awk '/prodige_name/ { print $2 }' ${PARAM_FILE}))
    host_bdd=$(eval echo $(awk '/prodige_host/ { print $2 }' ${PARAM_FILE}))
}

function set_db {
    logOkStep "(${CATALOGUE_DB}) Setting accs_adresse,accs_adresse_admin,accs_adresse_tele,accs_adresse_data in catalogue.acces_serveur"
    docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c 'select accs_adresse,accs_adresse_admin,accs_adresse_tele,accs_adresse_data from catalogue.acces_serveur'
    docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "update catalogue.acces_serveur set accs_adresse = '${CARTO}${DOMAIN}', accs_adresse_admin='${ADMINCARTO}${DOMAIN}', accs_adresse_tele='${TELECARTO}${DOMAIN}',accs_adresse_data='${DATACARTO}${DOMAIN}';"
    docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c 'select accs_adresse,accs_adresse_admin,accs_adresse_tele,accs_adresse_data from catalogue.acces_serveur'

    logOkStep "(${CATALOGUE_DB}) Setting system/server/host in public.settings"
    docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "select value from public.settings where name='system/server/host'"
    docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "update public.settings set value='${CATALOGUE}${DOMAIN}' where name='system/server/host';"
    docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "select value from public.settings where name='system/server/host'"

    logOkStep "(${CATALOGUE_DB}) Setting metadata/resourceIdentifierPrefix in public.settings"
    docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "select value from public.settings where name='metadata/resourceIdentifierPrefix'"
    docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "update public.settings set value='https://${CATALOGUE}${DOMAIN}/geonetwork/srv/' where name='metadata/resourceIdentifierPrefix';"
    docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "select value from public.settings where name='metadata/resourceIdentifierPrefix'"

    logOkStep "(${PRODIGE_DB}) Setting account_hostpostgres, account_passwordpostgres, account_userpostgres, account_dbpostgres in carmen.account"
    docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "select account_hostpostgres, account_passwordpostgres, account_userpostgres, account_dbpostgres from carmen.account"
    docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "update carmen.account set account_hostpostgres = '${host_bdd}', account_passwordpostgres = '${pass_bdd}',  account_userpostgres = '${user_bdd}' where account_dbpostgres = '${PRODIGE_DB}';"
    docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "select account_hostpostgres, account_passwordpostgres, account_userpostgres, account_dbpostgres from carmen.account"

    TRAILINGDOMAIN=${DOMAIN:1:${#DOMAIN}}
    logOkStep "(${PRODIGE_DB}) Setting dns_url in carmen.lex_dns"
    docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "select dns_url from carmen.lex_dns"
    docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "update carmen.lex_dns set dns_url = '${TRAILINGDOMAIN}';"
    docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "select dns_url from carmen.lex_dns"
}

function change_db {
    OLD=(${OLDADMINCARTO} ${OLDADMINSITE} ${OLDCARTO} ${OLDCAS} ${OLDCATALOGUE} ${OLDDATACARTO} ${OLDMAPSERV} ${OLDPRINT} ${OLDTABLE} ${OLDTELECARTO})
    NEW=(${ADMINCARTO} ${ADMINSITE} ${CARTO} ${CAS} ${CATALOGUE} ${DATACARTO} ${MAPSERV} ${PRINT} ${TABLE} ${TELECARTO})
    # Before
    # docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "select data from public.metadata"
    logOkStep "(${PRODIGE_DB} before) layer_metadata_file from carmen.layer"
    docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "select layer_metadata_file from carmen.layer"
    logOkStep "(${PRODIGE_DB} before) wxs_url from carmen.wxs"
    docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "select wxs_url from carmen.wxs"
    for i in ${!OLD[@]}; do
        logOkStep "(${CATALOGUE_DB}) Replacing ${OLD[i]}$OLDDOMAIN by ${NEW[i]}$DOMAIN in public.metadata"
        # docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "select data from public.metadata"
        docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "update public.metadata set data = replace(data, '${OLD[i]}$OLDDOMAIN', '${NEW[i]}$DOMAIN');"

        logOkStep "(${PRODIGE_DB}) Replacing ${OLD[i]}$OLDDOMAIN by ${NEW[i]}$DOMAIN in layer_metadata_file from carmen.layer"
        docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "update carmen.layer set layer_metadata_file = replace(layer_metadata_file, '${OLD[i]}$OLDDOMAIN', '${NEW[i]}$DOMAIN');"

        logOkStep "(${PRODIGE_DB}) Replacing ${OLD[i]}$OLDDOMAIN by ${NEW[i]}$DOMAIN in wxs_url from carmen.wxs"
        docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "update carmen.wxs set wxs_url = replace(wxs_url, '${OLD[i]}$OLDDOMAIN', '${NEW[i]}$DOMAIN');"
    done
    # After
    # docker exec -u postgres prodige_database psql -d ${CATALOGUE_DB} -c "select data from public.metadata"
    logOkStep "(${PRODIGE_DB} after) layer_metadata_file from carmen.layer"
    docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "select layer_metadata_file from carmen.layer"
    logOkStep "(${PRODIGE_DB} after) wxs_url from carmen.wxs"
    docker exec -u postgres prodige_database psql -d ${PRODIGE_DB} -c "select wxs_url from carmen.wxs"
}

function ldap_password {
    if ! ${DISABLELDAP}
    then
        bash ${SCRIPTDIR}/set_ldap_password.sh
        ifCmdFailStep "Set ldap password script failed : ${SCRIPTDIR}/set_ldap_password.sh"
    else
        logOkStep "Run this on the ldap host:"
        echo "bash ${SCRIPTDIR}/set_ldap_password.sh"
    fi
}
########################################################################################################################################

##################################
# main                           #
##################################

MODE="main"
logOk "############################"
logOk "########### main ###########"
logOk "############################"
# Options
OPTS=$( getopt -o h,d:,a:,s:,r:,k:,c:,q:,m:,p:,t:,e:,y,D:,A:,S:,R:,K:,C:,Q:,M:,P:,T:,E: -l help,domain:,admincarto:,adminsite:,carto:,cas:,catalogue:,datacarto:,mapserv:,print:,table:,telecarto:,yes,olddomain:,oldadmincarto:,oldadminsite:,oldcarto:,oldcas:,oldcatalogue:,olddatacarto:,oldmapserv:,oldprint:,oldtable:,oldtelecarto:,disableldap -- "$@" )
ifCmdFail "Option not found, use \"$SCRIPT -h\""
eval set -- "$OPTS"
while true
do
    case "$1" in
        -h | --help)
        usage
        exit 0
        ;;
        -d | --domain)
        DOMAIN=$2
        shift 2
        ;;
        -a | --admincarto)
        ADMINCARTO=$2
        shift 2
        ;;
        -s | --adminsite)
        ADMINSITE=$2
        shift 2
        ;;
        -r | --carto)
        CARTO=$2
        shift 2
        ;;
        -k | --cas)
        CAS=$2
        shift 2
        ;;
        -c | --catalogue)
        CATALOGUE=$2
        shift 2
        ;;
        -q | --datacarto)
        DATACARTO=$2
        shift 2
        ;;
        -m | --mapserv)
        MAPSERV=$2
        shift 2
        ;;
        -p | --print)
        PRINT=$2
        shift 2
        ;;
        -t | --table)
        TABLE=$2
        shift 2
        ;;
        -e | --telecarto)
        TELECARTO=$2
        shift 2
        ;;
        -y | --yes)
        YES=true
        shift
        ;;
        -D | --olddomain)
        OLDDOMAIN=$2
        shift 2
        ;;
        -A | --oldadmincarto)
        OLDADMINCARTO=$2
        shift 2
        ;;
        -S | --oldadminsite)
        OLDADMINSITE=$2
        shift 2
        ;;
        -R | --oldcarto)
        OLDCARTO=$2
        shift 2
        ;;
        -K | --oldcas)
        OLDCAS=$2
        shift 2
        ;;
        -C | --oldcatalogue)
        OLDCATALOGUE=$2
        shift 2
        ;;
        -Q | --olddatacarto)
        OLDDATACARTO=$2
        shift 2
        ;;
        -M | --oldmapserv)
        OLDMAPSERV=$2
        shift 2
        ;;
        -P | --oldprint)
        OLDPRINT=$2
        shift 2
        ;;
        -T | --oldtable)
        OLDTABLE=$2
        shift 2
        ;;
        -E | --oldtelecarto)
        OLDTELECARTO=$2
        shift 2
        ;;
        --disableldap)
        DISABLELDAP=true
        shift
        ;;
        --)
        shift
        break
        ;;
    esac
done

if [[ $(id -u) -ne 0 ]] ; then
    logError "You must be root or sudo to execute $SCRIPTNAME"
    exit 1
fi

# Step

NB_STEP_AL=5

STEP_AL=1
MODE="Validation"
logOkStep "Validation of variables"
validation

STEP_AL=2
MODE="GetDatabaseParameters"
logOkStep "Get database parameters"
get_db_parameters

STEP_AL=3
MODE="SetDatabaseValues"
logOkStep "Set database values"
set_db

STEP_AL=4
MODE="ReplaceDatabaseValues"
logOkStep "Replace database values"
change_db

STEP_AL=5
MODE="SetLdapPassword"
logOkStep "Set ldap password"
ldap_password

logOk "End of ${SCRIPTNAME}"
# End
