#!/bin/bash

# Empty /backup directory: create tablespace and database
# Non-empty /backup directory: load /backup/*.sql and /backup/*.dmp in order

# Variables:
PGDATA="/var/lib/postgresql/data"
POSTGRES_PRODIGE_TABLESPACE="tab_${DB_PRODIGE_NAME}"
POSTGRES_CATALOGUE_TABLESPACE="tab_${DB_CATALOGUE_NAME}"

# Create directories for tablespace
mkdir -p ${PGDATA}/base/${POSTGRES_PRODIGE_TABLESPACE}
mkdir -p ${PGDATA}/base/${POSTGRES_CATALOGUE_TABLESPACE}

if [ -z "$(ls -A /backup)" ]
then
  echo "INIT-DB: === Simple init database creation (empty /backup directory) ==="
  echo "INIT-DB: === USER creation ==="
  echo "Role : ${USER_NAME}"
  psql -c "CREATE ROLE ${USER_NAME} PASSWORD '${USER_PASSWORD}' NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN;"
  psql -c "ALTER ROLE ${USER_NAME} SUPERUSER;"

  if [ -n "${DB_PRODIGE_NAME}" -a -n "${DB_CATALOGUE_NAME}" ]
  then
    echo "INIT-DB: === PRODIGE creation ==="
    # Base de données
    echo "Database : ${DB_PRODIGE_NAME}"
    psql -c "CREATE TABLESPACE \"${POSTGRES_PRODIGE_TABLESPACE}\" OWNER \"${USER_NAME}\" LOCATION '${PGDATA}/base/${POSTGRES_PRODIGE_TABLESPACE}';"
    createdb -T template0 -D "${POSTGRES_PRODIGE_TABLESPACE}" -O "${USER_NAME}" "${DB_PRODIGE_NAME}"

    # Add Postgis
    psql -d "${DB_PRODIGE_NAME}" -c "CREATE EXTENSION postgis;"
    # Grant insert, update, delete on spatial_ref_sys to ${USER_NAME}
    psql -d "${DB_PRODIGE_NAME}" -c "GRANT INSERT, UPDATE, DELETE ON spatial_ref_sys TO ${USER_NAME};"

    echo "INIT-DB: === CATALOGUE creation ==="

    # Base de données
    echo "Database : ${DB_CATALOGUE_NAME}"
    psql -c "CREATE TABLESPACE \"${POSTGRES_CATALOGUE_TABLESPACE}\" OWNER \"${USER_NAME}\" LOCATION '${PGDATA}/base/${POSTGRES_CATALOGUE_TABLESPACE}';"
    createdb -T template0 -D "${POSTGRES_CATALOGUE_TABLESPACE}" -O "${USER_NAME}" "${DB_CATALOGUE_NAME}"

    # Add Postgis
    psql -d "${DB_CATALOGUE_NAME}" -c "CREATE EXTENSION postgis;"
  else
    echo "INIT-DB: === Neither PRODIGE nor CATALOGUE creation (empty variable DB_PRODIGE_NAME: ${DB_PRODIGE_NAME} or DB_CATALOGUE_NAME: ${DB_CATALOGUE_NAME}) ==="
  fi
  psql -c "ALTER ROLE ${USER_NAME} NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT LOGIN;"
else
  # Manage backup import
  # Find .sql and .dmp backup file in /backup dir
  echo "INIT_DB: === Restore databases from non-empty /backup instead of simple init ==="
  for fullfile in $(find /backup -maxdepth 1 -type f -name '*.sql' -o -name '*.dmp' | sort -V); do
    echo "INIT-DB: Start import $fullfile"
    filename=$(basename -- "$fullfile")
    extension="${filename##*.}"
    filename="${filename%.*}"

    # Detect file type
    filetype=$(file "$fullfile"| awk '{print $2}')
    if [ "$filetype" == "ASCII" ]
    then
        echo "INIT-DB: sql restore"
        psql -f $fullfile || true
    else
        echo "INIT-DB: dmp restore"
        pg_restore -v -C -d postgres $fullfile || true
    fi
  done
fi
