<div class="border-bottom">
  <div id="title" class="title col-12 text-center m-0">
    {if $title_alias != '' && $title==''}
      <b>{$title_alias}</b>
    {else}
      <h3>{$title}</h3>
    {/if}
  </div>
  <div id="subtitle" class="col-12 text-center">
    {if $subtitle_alias != '' && $subtitle==''}
      <b>{$subtitle_alias}</b>
    {else}
      <small>{$subtitle}</small>
    {/if}
  </div>
</div>

{if $pageNumber==1}
  
  <div class="row m-0 map-row">
    <div id="map-container" class="col-8 p-0">
      {if $map_alias != '' && $map==''}
        <b>{$map_alias}</b>
      {else}
        {$map}
      {/if}
      <div id="keymap">
        {if $keymap_alias != '' && $keymap==''}
          <b>{$keymap_alias}</b>
        {else}
          {$keymap}
        {/if}
      </div>
      <div>
        {if $orientation=='landscape'}
          <div class="landscape">
            {if $scaleline_alias != '' && $scaleline==''}
              <b>{$scaleline_alias}</b>
            {else}
              {$scaleline}
            {/if}
          </div>
        {else}
          <div class="portrait">
            {if $scaleline_alias != '' && $scaleline==''}
              <b>{$scaleline_alias}</b>
            {else}
              {$scaleline}
            {/if}
          </div>
        {/if}
        {if $mentionCheckbox!=''}
          <div class="workDocument">
            {if $mentionText!=''}
              {$mentionText}
            {/if}
          </div>
        {/if}
      </div>

      {if $comment_alias != '' && $comment==''}
        <b>{$comment_alias}</b>
      {else}
        {if $comment!=''}
          <div class="border-top">
            <div id="comment" class="p-2 ml-1 text-left">
                {$comment}
            </div>
          </div>
        {/if}
      {/if}

      <div class="border-top">
        <div class="p-2 ml-1">
          {if $copyright_alias != '' && $copyright==''}
            <b>{$copyright_alias}</b>
          {else}
            {$copyright}
          {/if}
          {if $logo_alias != '' && $logo==''}
            <b>{$logo_alias}</b>
          {else}
            {$logo}
          {/if}
        </div>
      </div>

    </div>
    <div class="col-4 p-0 m-0 border-left">
      <div class="p-0">
        {if $legend_alias != '' && $legend==''}
          <b>{$legend_alias}</b>
        {else}
          {$legend}
        {/if}
      </div>
    </div>
  </div>

{else}
  
  <div class="row map-row">
    <div id="map-container" class="col-12 row h-100">
      {if $map_alias != '' && $map==''}
        <b>{$map_alias}</b>
      {else}
        {$map}
      {/if}
      <div id="keymap">
        {if $keymap_alias != '' && $keymap==''}
          <b>{$keymap_alias}</b>
        {else}
          {$keymap}
        {/if}
      </div>
      <div>
        {if $orientation=='landscape'}
          <div class="landscape">
            {if $scaleline_alias != '' && $scaleline==''}
              <b>{$scaleline_alias}</b>
            {else}
              {$scaleline}
            {/if}
          </div>
        {else}
          <div class="portrait">
            {if $scaleline_alias != '' && $scaleline==''}
              <b>{$scaleline_alias}</b>
            {else}
              {$scaleline}
            {/if}
          </div>
        {/if}
        {if $mentionCheckbox!=''}
          <div class="workDocument">
            {if $mentionText!=''}
              {$mentionText}
            {/if}
          </div>
        {/if}
      </div>
    </div>
  </div>

  <div class="border-top">
    <div id="comment" class="p-2 ml-1 text-left">
      {if $comment_alias != '' && $comment==''}
        <b>{$comment_alias}</b>
      {else}
        {$comment}
      {/if}
    </div>
  </div>

  <div class="border-top">
    <div class="p-2 ml-1">
      {if $copyright_alias != '' && $copyright==''}
        <b>{$copyright_alias}</b>
      {else}
        {$copyright}
      {/if}
      {if $logo_alias != '' && $logo==''}
        <b>{$logo_alias}</b>
      {else}
        {$logo}
      {/if}
    </div>
  </div>

  <div style="page-break-before:always">
    <div class="border-bottom">
      <div id="title" class="title col-12 text-center m-0">
        {if $title_alias != '' && $title==''}
          <b>{$title_alias}</b>
        {else}
          <h3>{$title}</h3>
        {/if}
      </div>
      <div id="subtitle" class="col-12 text-center">
        {if $subtitle_alias != '' && $subtitle==''}
          <b>{$subtitle_alias}</b>
        {else}
          <small>{$subtitle}</small>
        {/if}
      </div>
    </div>

    <div class="row map-row">
      <div class="col-12 p-0 mx-3">
        <div class="p-0">
          {if $legend_alias != '' && $legend==''}
            <b>{$legend_alias}</b>
          {else}
            {$legend}
          {/if}
        </div>
      </div>
    </div>
  </div>

{/if}
