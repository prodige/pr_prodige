<div class="border-bottom">
  <div id="title" class="title col-12 text-center m-0">
    {if $title_alias != '' && $title==''}
      <b>{$title_alias}</b>
    {else}
      <h3>{$title}</h3>
    {/if}
  </div>
</div>

<div class="row m-0 map-row">
  <div class="col-4 p-0 m-0 border-right">

    <div class="row">
      <div class="col-12">
        {if $keymap_alias != '' && $keymap==''}
          <b>{$keymap_alias}</b>
        {else}
          {$keymap}
        {/if}
      </div>
    </div>

    <div class="row mt-2">
      <div class="col-12">
        {if $legend_alias != '' && $legend==''}
          <b>{$legend_alias}</b>
        {else}
          {$legend}
        {/if}
      </div>
    </div>

  </div>
  <div id="map-container" class="col-8 p-0">

    {if $map_alias != '' && $map==''}
      <b>{$map_alias}</b>
    {else}
      {$map}
    {/if}

    {if $comment_alias != '' && $comment==''}
      <b>{$comment_alias}</b>
    {else}
      {if $comment!=''}
        <div class="border-top">
          <div id="comment" class="p-2 ml-1 text-left">
              {$comment}
          </div>
        </div>
      {/if}
    {/if}

    <div class="border-top">
      <div class="p-2 ml-1">
        {if $copyright_alias != '' && $copyright==''}
          <b>{$copyright_alias}</b>
        {else}
          {$copyright}
        {/if}
        {if $logo_alias != '' && $logo==''}
          <b>{$logo_alias}</b>
        {else}
          {$logo}
        {/if}
      </div>
    </div>

  </div>
</div>
