server {
    listen 80;
    server_name catalogue.prodige.internal;

    rewrite ^ https://catalogue.prodige.internal$request_uri? permanent;
}

server {
    listen 443 ssl http2;
    server_name catalogue.prodige.internal;
    client_max_body_size 0;

    ssl_certificate /etc/nginx/certs/prodige.crt;
    ssl_certificate_key /etc/nginx/certs/prodige.key;
    ssl_stapling on;


    location ~ "/geonetwork/srv/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}" {
      rewrite /geonetwork/srv/(.*) /geonetwork/srv/api/records/$1/formatters/xml break;
    }

    location /geonetwork {
      proxy_set_header Host $http_host;
      proxy_set_header X-Real-IP $remote_addr;
      proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
      proxy_set_header X-Forwarded-Port 443;
      proxy_set_header X-Forwarded-Proto $scheme;
      resolver 127.0.0.11 valid=10s;
      set $geonetwork prodige-geonetwork;
      proxy_pass https://$geonetwork:8443;
      proxy_ssl_verify off;
      proxy_ssl_trusted_certificate /etc/nginx/certs/prodige.crt;
    }

    location / {
      proxy_set_header Host $host;
      proxy_set_header X-Forwarded-For $proxy_protocol_addr;
      proxy_set_header X-Forwarded-Port 443;
      proxy_set_header X-Forwarded-Proto "https";
      proxy_set_header X-Real-IP $remote_addr;
      resolver 127.0.0.11 valid=10s;
      set $catalogue prodige-catalogue;
      proxy_pass http://$catalogue:80;
    }
}
