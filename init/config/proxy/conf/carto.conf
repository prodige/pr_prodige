server {
    listen 80;
    server_name carto.prodige.internal;

    location / {
        return 301 https://$http_host$request_uri;
    }

    location ~ "/.well-known/acme-challenge/([\w-]{43})" {
        default_type "text/plain";
        root /var/www/letencrypt;
    }

}

server {
    listen 443 ssl http2;
    server_name carto.prodige.internal;
    client_max_body_size 0;

    ssl_certificate /etc/nginx/certs/prodige.crt;
    ssl_certificate_key /etc/nginx/certs/prodige.key;
    ssl_stapling on;

    location ~ ^/(carto|core|mapimage|IHM|CartesStatiques) {
      proxy_set_header Host $host;
      proxy_set_header X-Forwarded-For $proxy_protocol_addr;
      proxy_set_header X-Forwarded-Port 443;
      proxy_set_header X-Forwarded-Proto "https";
      proxy_set_header X-Real-IP $remote_addr;
      resolver 127.0.0.11 valid=10s;
      set $visualiseurback ppr_prodige_visualiseur_web;
      proxy_pass http://$visualiseurback:80;
    }

    location / {
      proxy_set_header Host $host;
      proxy_set_header X-Forwarded-For $proxy_protocol_addr;
      proxy_set_header X-Forwarded-Port 443;
      proxy_set_header X-Forwarded-Proto "https";
      proxy_set_header X-Real-IP $remote_addr;
      resolver 127.0.0.11 valid=10s;
      set $visualiseur "http://ngpr-prodige-visualiseur_web:80";
      proxy_pass $visualiseur;
    }
}
