(function (window) {
  window.__env = window.__env || {};

  /**
   * Ce fichier permet de surcharger les données d'environement du fichier env.service.
   * Décommenter uniquement ce qui doit être surchargé.
   */

  window.__env.isPrintMode = true;
  window.__env.serverUrl = 'https://carto.prodige.internal/';
  // window.__env.prodige41Url = 'https://';
  // window.__env.frontCartoUrl = 'https://';

  // window.__env.interrogationPointPrecision = 5;
  // window.__env.interrogationTooltipPrecision = 5;
  // window.__env.defaultMousePositionProjection = 'EPSG:4326';
  /* window.__env.availableProjections = {
    4326: {
      key: 'EPSG:4326',
      label: 'WGS84',
      proj: '+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs',
      bounds: [-180.0000, -90.0000, 180.0000, 90.0000]
    }, ...
  }; */
}(this));
