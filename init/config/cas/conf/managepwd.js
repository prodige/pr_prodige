/**
 * Prodige5 forgotten password utility
 * @author alkante <support@alkante.com>
 */

/**
 * Opens the forgotten password popup
 */

function openPopupForgotPwd() {
  var width   = 800;
  var left    = (screen.width/2)-(width/2);
  var baseUrl = 'https://catalogue.prodige.internal/admin/chgpwd';
  var login   = $('#username').val();

  window.open(
      baseUrl+'?login='+login,
      'Reset password',
      'dialog=yes,location=no,scrollbars=yes,titlebar=no,height=400,top=300,width='+width+',left='+left
  );
}

function openPopupChangePwd() {
  var width   = 800;
  var left    = (screen.width/2)-(width/2);
  var baseUrl = 'https://catalogue.prodige.internal/admin/chgpwd';
  var login   = typeof($('#username').val()) !== 'undefined' ? $('#username').val() : '';

  window.open(
      baseUrl+'?login='+login,
      'Reset password',
      'dialog=yes,location=no,scrollbars=yes,titlebar=no,height=400,top=300,width='+width+',left='+left
  );
}
