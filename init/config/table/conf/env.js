(function (window) {
  window.__env = window.__env || {};

  //window.__env.production = true;
  window.__env.frontCartoUrl = 'https://carto.prodige.internal/';
  window.__env.catalogueUrl = 'https://catalogue.prodige.internal';
  window.__env.geonetworkUrl = 'https://catalogue.prodige.internal/geonetwork';
}(this));
