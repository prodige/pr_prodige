
server {
    listen *:80;
    listen [::]:80;
    server_name datacarto.prodige.internal;
    location / {
        return 301 https://$http_host$request_uri;
    }
}

server {
    listen *:443 ssl;
    listen [::]:443 ssl;
    server_name datacarto.prodige.internal;
    ignore_invalid_headers off;
    client_max_body_size 0;

    location = /favicon.ico {
        log_not_found off;
        access_log off;
        try_files $uri @proxy;
    }

    location @proxy {
        proxy_pass "http://127.0.0.1:15007";
        proxy_redirect          off;
        proxy_set_header        Host               $host;
        proxy_set_header        X-Real-IP          $remote_addr;
        proxy_set_header        X-Forwarded-For    $proxy_add_x_forwarded_for;
        proxy_set_header        X-Forwarded-Host   $host;
        proxy_set_header        X-Forwarded-Server $host;
        proxy_set_header        X-Forwarded-Port   $server_port;
        proxy_set_header        X-Forwarded-Proto  $scheme;
        client_body_buffer_size 128k;
        proxy_connect_timeout   90;
        proxy_send_timeout      90;
        proxy_read_timeout      90;
        proxy_buffers           32 4k;
    }

    location / {
        try_files $uri @proxy;
    }

    # SSL
    ssl_certificate /etc/ssl/certs/datacarto.prodige.internal.chain.crt;
    ssl_certificate_key /etc/ssl/private/datacarto.prodige.internal.key;

    # Logs
    error_log /var/log/nginx/datacarto.prodige.internal.error.log;
    access_log /var/log/nginx/datacarto.prodige.internal.access.log;

}
