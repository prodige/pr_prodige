# Tests

## Démarrage progressif des dockers

```bash
sudo ./bin/make_init_and_rights.sh

docker login docker.alkante.com
```

Puis démarrage avec le docker-compose et les fichiers de surcharge choisis dans l'ordre suivant des dockers :

```bash
docker-compose up -d openldap prodige_catalogue
```

puis

```bash
docker-compose up -d jpr_prodige_cas_web
```

puis

```bash
docker-compose up -d jpr_prodige_geonetwork_web
```

et enfin démarrage de tous les dockers.
```bash
docker-compose up -d
```

## Test des services

Utiliser login/password :  admin@prodige-opensource.org / prodige01

| Service                | Images                          | URL |
| ---------------------- | ------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| openldap               | jpr_prodige_cas_ldap            | cf doc ldap                                                                                                                                                                  |
| postgresql             | prodige_database                | cf documentation postgresql                                                                                                                                                  |
| cas                    | jpr_prodige_cas_web             | https://cas.prodige.internal                                                                                                                                                 |
| geonetwork             | jpr_prodige_geonetwork_web      | https://catalogue.prodige.internalgeonetwork                                                                                                                                 |
| catalogue              | ppr_prodige_catalogue_web       | https://catalogue.prodige.internal  ??                                                                                                                                       |
| adminsite              | ppr_prodige_adminsite_web       | https://adminsite.prodige.internal                                                                                                                                           |
| admincarto             | ppr_prodige_admincarto_web      | https://cas.prodige.internal puis https://admincarto.prodige.internal/prodige/connect puis https://admincarto.prodige.internal/edit_map/617af7f0-0a6f-11de-ad6b-00104b7907b4 |
| telecarto              | ppr_prodige_telecarto_web       | https://telecarto.prodige.internal ??                                                                                                                                        |
| visualiseur            | ngpr-prodige-visualiseur_web    | https://carto.prodige.internal/1/limites_administratives.map                                                                                                                 |
| visualiseur-back       | ppr_prodige_visualiseur_web     | https://carto.prodige.internal/carto https://carto.prodige.internal/core ??                                                                                                  |
| visualiseur-core-print | ngpr_visualiseur_core_print_web | https://print.prodige.internal                                                                                                                                               |
| table                  | ngpr_prodige_table_editeur_web  | https://table.prodige.internal ??                                                                                                                                            |
| datacarto              | ppr_prodige_datacarto_web       | https://datacarto.prodige.internal ??                                                                                                                                        |
| mapserver              | ppr_prodige_mapserver_api_web   | https://mapserv.prodige.internal ??                                                                                                                                          |


Tester les fichiers `.map` dans datacarto :

```bash
shp2img -m /var/www/html/data/cartes/Publication/limites_administratives.map -all_debug 5 -o /tmp/test.png
shp2img -m /var/www/html/data/cartes/Publication/wms.map -all_debug 5 -o /tmp/test.png
```
