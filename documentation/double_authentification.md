# Double authentification
Lancer le projet une première fois sans la double authentification parmi les 4 choix possibles, voir [README.md](README.md).

Modifier l'adresse mail de l'administrateur de la plateforme admin@prodige-opensource.org, le login reste admin@prodige-opensource.org.

## Prérequis
Un relay mail de configurer et qui fonctionne.

Voir les options de [./bin/set_prodige_parameters.sh](../bin/set_prodige_parameters.sh)


## Activation de la double authentification
Lancer le script [./bin/set_prodige_parameters.sh](../bin/set_prodige_parameters.sh) avec les mêmes arguments que la première fois en activant cette fois-ci la double authentification.

Ou lancer le script avec l'option ```--double-auth```.
