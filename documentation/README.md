# 4 modes d'installation de Prodige avec les conteneurs Docker

## Caractéristiques communes

 - Utilisation de docker-compose pour la gestion des dockers composant Prodige.
 - Les volumes ./data conservent les paramétrages et données persistantes ;
 - Les volumes ./var correspondent aux données persistantes qui n'ont pas besoin d'être sauvegardées (journaux, imports).

## A - Installation pour du test ou du développement

 - Utilise [docker-compose.yml](../docker-compose.yml) et [docker-compose.override.yml](../docker-compose.override.yml) avec un docker proxy (défini dans [docker-compose.override.yml](../docker-compose.override.yml))pour les configurations Nginx et les certificats.
 - Les conteneurs sont téléchargés.
 - Voir fichier [installation.md](installation.md)

## B - Installation sur serveur de production

 - Utilise [docker-compose.yml](../docker-compose.yml) et [production.yml](../docker-compose-custom/production.yml)
 - Les conteneurs sont téléchargés.
 - Voir fichier [installation_production.md](installation_production.md)

## C - Installation sur serveur de production sans le docker proxy

 - Utilise [docker-compose.yml](../docker-compose.yml), [production.yml](../docker-compose-custom/production.yml) et [docker-compose.nginx.yml](../nginx/docker-compose.nginx.yml)
 - Les conteneurs sont téléchargés.
 - Les certificats et la configuration du serveur web sont gérés en dehors du projet pr_prodige.
 - Voir fichier [installation_production_avec_nginx.md](installation_production_avec_nginx.md)

## D - Installation avec une construction locale des conteneurs

 - Utilise [docker-compose.yml](../docker-compose.yml) [docker-compose.override.yml](../docker-compose.override.yml) et [local_build.yml](../docker-compose-custom/local_build.yml).
 - Les conteneurs sont construits localement.
 - Les tags des dockers sont à `latest` (objet de [local_build.yml](../docker-compose-custom/local_build.yml)).
 - Voir fichier [installation_construction_locale.md](installation_construction_locale.md)

## Liste des dockers produits par sous-projet prodige et spécification des technologies:

Liste exécutée par docker-compose ():
- [x] alkante/jpr_prodige_cas
  - jpr_prodige_cas_web: Docker Java/Tomcat8
  - jpr_prodige_cas_ldap: Docker openldap
- [x] alkante/jpr_prodige_geosource
  - jpr_prodige_geosource_web: DockerJava/Tomcat8
- [x] alkante/ppr_prodige_adminsite:
  - ppr_prodige_adminsite_web: Docker PHP/Symfony3
- [x] alkante/ppr_prodige_admincarto
  - ppr_prodige_admincarto_web: Docker PHP/Symfony3
- [x] alkante/ppr_prodige_mapserver_api
  - ppr_prodige_mapserver_api_web: Docker PHP/Symfony3
- [x] alkante/ppr_prodige_catalogue
  - ppr_prodige_catalogue_web: Docker PHP/Symfony3
- [x] alkante/ppr_prodige_telecarto
  - ppr_prodige_telecarto_web: Docker PHP/Symfony3
- [x] alkante/bdl_prodige_core_cas
  - prodige/cas: Bundle PHP/Symfony3
- [x] alkante/bdl_prodige_core
  - prodige/core: Bundle PHP/Symfony3
- [x] alkante/ppr_prodige_datacarto
  - ppr_prodige_datacarto_web
- [x] alkante/ppr_prodige_visualiseur
  - ppr_prodige_visualiseur: PHP/Phalcon
- [x] alkante/bdl_visualiseur_core
  - alkante/bdl_visualiseur_core: Bundle PHP
- [x] alkante/ngpr_visualiseur_core_print
  - ngpr_visualiseur_core_print_web: JS/Angular
- [x] angular/ngpr-prodige-visualiseur
  - ngpr-prodige-visualiseur_web: JS/Angular
- [x] angular/bdl-alkante-visualiseur-core
  - @alkante/visualiseur-core: Packet Angular
- [x] alkante/ngpr_prodige_table_editeur
  - ngpr_prodige_table_editeur_web: JS/Angular
