# Installation de Prodige en local

## Principes

 - Utilise docker-compose.yml (pour les dockers Prodige) et docker-compose.override.yml (pour le proxy Nginx) qui installent par défaut le domaine `.prodige.internal`.

## Initialisation des volumes de données

 - data
 - var

En tant que `root` ou avec `sudo` :

```bash
sudo ./bin/make_init_and_rights.sh
```

## Connexion au docker registry
- Utilisateur : prodige
- Mot de passe : Yx@KN,ZXzR9u@

```bash
docker login docker.alkante.com -u prodige
docker-compose pull
```

## Domaines

Si les domaines ne sont pas publics (tests), ajouter dans `/etc/hosts` une ligne du type :

```
127.0.0.2 cas.prodige.internal admincarto.prodige.internal adminsite.prodige.internal mapserv.prodige.internal catalogue.prodige.internal carto.prodige.internal datacarto.prodige.internal telecarto.prodige.internal print.prodige.internal table.prodige.internal phppgadmin.prodige.internal
```

Pour utiliser un autre domaine que le domaine par défaut ci-dessus et s'il n'est pas public, il faut aussi changer les `aliases` du service `prodige-proxy` dans le docker-compose.override.yml (ou ajouter un autre fichier d'« override » comme production.yml ou local_build.yml, voir [documentation Docker compose sur les fichiers de surcharge](https://docs.docker.com/compose/extends/)).

## Démarrage de Prodige (domaine .prodige.internal)
Lancement des bases de données
```bash
docker-compose up -d prodige_database jpr_prodige_cas_ldap
```
Lancement des applicatifs
```bash
docker-compose up -d
```

## Journaux
```bash
docker-compose logs -f
```

## Vérification du bon démarrage
Les containers vont mettre un certain temps de démarrage. Voici des commandes pour vérifier qu'ils ont fini le démarrage.
### Base de données
BDD postgres :
```bash
docker-compose logs prodige_database
...
LOG:  le système de bases de données est prêt pour accepter les connexions
```
BDD ldap :
```bash
docker-compose logs jpr_prodige_cas_ldap
...
XXXXXX slapd starting
```

### App prodige
Admincarto (migration de bdd):
```bash
docker-compose logs ppr_prodige_admincarto_web
...
++ finished in X.Xs
++ X migrations executed
++ XXXX sql queries
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Adminsite :
```bash
docker-compose logs ppr_prodige_adminsite_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
CAS :
```bash
docker-compose logs jpr_prodige_cas_web
...
- <Configuration files found at [/etc/cas/config] are [[/etc/cas/config/cas.properties]] under profile(s) [[standalone]]>
...
```
Catalogue (migration de bdd):
```bash
docker-compose logs ppr_prodige_catalogue_web
...
++ finished in X.Xs
++ X migrations executed
++ XXXX sql queries
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Datacarto :
```bash
docker-compose logs ppr_prodige_datacarto_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Geonetwork :
```bash
docker-compose logs jpr_prodige_geonetwork_web
...
- Data directory provided could not be used. Using default location: /usr/local/tomcat/webapps/geonetwork/WEB-INF/data
...
```
Mapserver :
```bash
docker-compose logs ppr_prodige_mapserver_api_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Proxy
```bash
docker-compose logs prodige-proxy
...
1#1: start worker processes
...
```
Telecarto
```bash
docker-compose logs ppr_prodige_telecarto_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Visualiseur
```bash
docker-compose logs ppr_prodige_visualiseur_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```

## Arrêt

```bash
docker-compose down
```
