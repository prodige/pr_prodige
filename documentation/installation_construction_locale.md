# Déploiement de Prodige par construction locale des dockers

Placez-vous dans un répertoire pour votre projet, par exemple :

```bash
mkdir prodige
cd prodige
```

## Récupérer les projets
```bash
git clone https://gitlab.adullact.net/prodigeadmin/jpr_prodige_cas
git clone https://gitlab.adullact.net/prodigeadmin/jpr_prodige_geonetwork
git clone https://gitlab.adullact.net/prodigeadmin/ppr_prodige_adminsite
git clone https://gitlab.adullact.net/prodigeadmin/ppr_prodige_admincarto
git clone https://gitlab.adullact.net/prodigeadmin/ppr_prodige_catalogue
git clone https://gitlab.adullact.net/prodigeadmin/ppr_prodige_telecarto
git clone https://gitlab.adullact.net/prodigeadmin/ppr_prodige_mapserver_api
git clone https://gitlab.adullact.net/prodigeadmin/ppr_prodige_datacarto
git clone https://gitlab.adullact.net/prodigeadmin/ppr_prodige_visualiseur
git clone https://gitlab.adullact.net/prodigeadmin/ngpr_visualiseur_core_print
git clone https://gitlab.adullact.net/prodigeadmin/ngpr-prodige-visualiseur
git clone https://gitlab.adullact.net/prodigeadmin/ngpr_prodige_table_editeur
```

## Construire les dockers des projets

Option : sans prompt, il faut paramétrer les variables d'environnement :

```bash
export NEXUS_USR=prodige
export NEXUS_PSW=Yx@KN,ZXzR9u@
```

```bash
cd jpr_prodige_cas/cicd/build
./1_build_war.sh
./6_make_docker.sh
# Vérification
docker images | egrep 'jpr_prodige_cas_web|jpr_prodige_cas_ldap'
cd ../../..

cd ppr_prodige_admincarto/cicd/build
./0_fetch-dependencies.sh      # Prompt password for nexus
./5_make_release.sh
./6_make_docker.sh
# Vérification
docker images | egrep 'ppr_prodige_admincarto_web'  
cd ../../..

cd ppr_prodige_catalogue/cicd/build
./0_fetch-dependencies.sh     # Prompt password for nexus
./1_asset_cache.sh
./5_make_release.sh
./6_make_docker.sh
# Vérification
docker images | egrep 'ppr_prodige_catalogue_web'  
cd ../../..

cd ppr_prodige_adminsite/cicd/build
./0_fetch-dependencies.sh     # Prompt password for nexus
./1_asset_cache.sh
./5_make_release.sh
./6_make_docker.sh
docker images | egrep 'ppr_prodige_adminsite_web'
cd ../../..

cd jpr_prodige_geonetwork/cicd/build
./0_fetch-dependencies.sh
./1_build_war.sh
./6_make_docker.sh
docker images | egrep 'jpr_prodige_geonetwork_web'
cd ../../..

cd ppr_prodige_telecarto/cicd/build
./0_fetch-dependencies.sh      # Prompt password for nexus
./1_asset_cache.sh
./5_make_release.sh
./6_make_docker.sh
docker images | egrep 'ppr_prodige_telecarto_web'
cd ../../..

cd ppr_prodige_mapserver_api/cicd/build
./0_fetch-dependencies.sh
./5_make_release.sh
./6_make_docker.sh
docker images | egrep 'ppr_prodige_mapserver_api_web'
cd ../../..

cd ppr_prodige_datacarto/cicd/build
./0_fetch-dependencies.sh
./5_make_release.sh
./6_make_docker.sh

docker images | egrep 'ppr_prodige_datacarto_web'
cd ../../..

cd ppr_prodige_visualiseur/cicd/build
./0_fetch-dependencies.sh
./5_make_release.sh
./6_make_docker.sh
docker images | egrep 'ppr_prodige_visualiseur_web'
cd ../../..


cd ngpr_visualiseur_core_print/cicd/build
./0_fetch-dependencies.sh
./3_build-prod.sh
./5_make_release.sh
./6_make_docker.sh
docker images | egrep 'ngpr_visualiseur_core_print_web'
cd ../../..


cd ngpr-prodige-visualiseur/cicd/build
./0_fetch-dependencies.sh
./3_build-prod.sh
./5_make_release.sh
./6_make_docker.sh
docker images | egrep 'ngpr-prodige-visualiseur_web'
cd ../../..


cd ngpr_prodige_table_editeur/cicd/build
./0_fetch-dependencies.sh
./3_build-prod.sh
./5_make_release.sh
./6_make_docker.sh
docker images | egrep 'ngpr_prodige_table_editeur_web'
cd ../../..

```

## Principes

 - Fonctionne de façon similaire à [installation.md](installation.md) avec le fichier [local_build.md](../local_build.md) en surcharge des paramètres des conteneurs à ```latest``` pour utiliser les constructions locales.
 - Utilise [docker-compose.yml](../docker-compose.yml) (pour les dockers Prodige), [docker-compose.override.yml](../docker-compose.override.yml) (pour le proxy Nginx) qui installent par défaut le domaine ```.prodige.internal``` et donc [local_build.md](../local_build.md).

## Initialisation des volumes de données

 - ./data
 - ./var

En tant que `root` ou `sudo` :

```bash
sudo ./bin/make_init_and_rights.sh
```

## Domaines

Si les domaines ne sont pas publics (tests), ajouter dans `/etc/hosts` une ligne du type :

```
127.0.0.2 cas.prodige.internal admincarto.prodige.internal adminsite.prodige.internal mapserv.prodige.internal catalogue.prodige.internal carto.prodige.internal datacarto.prodige.internal telecarto.prodige.internal print.prodige.internal table.prodige.internal
```

Pour utiliser un autre domaine que le domaine par défaut ci-dessus et s'il n'est pas public, il faut aussi changer les `aliases` du service `prodige-proxy` dans le [docker-compose.override.yml](../docker-compose.override.yml) (ou ajouter un autre fichier d'« override » comme [production.yml](../production.yml) ou [local_build.yml](../local_build.yml), voir [documentation Docker compose sur les fichiers de surcharge](https://docs.docker.com/compose/extends/)).

## Démarrage de Prodige (domaine .prodige.internal)
Lancement des bases de données
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml up -d prodige_database jpr_prodige_cas_ldap
```
Lancement des applicatifs
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml up -d
```

## Journaux
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs -f
```

## Vérification du bon démarrage
Les containers vont mettre un certain temps de démarrage. Voici des commandes pour vérifier qu'ils ont fini le démarrage.
### Base de données
BDD postgres :
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs prodige_database
...
LOG:  le système de bases de données est prêt pour accepter les connexions
```
BDD ldap :
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs jpr_prodige_cas_ldap
...
XXXXXX slapd starting
```

### App prodige
Admincarto (migration de bdd):
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs ppr_prodige_admincarto_web
...
++ finished in X.Xs
++ X migrations executed
++ XXXX sql queries
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Adminsite :
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs ppr_prodige_adminsite_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
CAS :
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs jpr_prodige_cas_web
...
- <Configuration files found at [/etc/cas/config] are [[/etc/cas/config/cas.properties]] under profile(s) [[standalone]]>
...
```
Catalogue (migration de bdd):
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs ppr_prodige_catalogue_web
...
++ finished in X.Xs
++ X migrations executed
++ XXXX sql queries
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Datacarto :
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs ppr_prodige_datacarto_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Geonetwork :
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs jpr_prodige_geonetwork_web
...
- Data directory provided could not be used. Using default location: /usr/local/tomcat/webapps/geonetwork/WEB-INF/data
...
```
Mapserver :
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs ppr_prodige_mapserver_api_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Proxy
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs prodige-proxy
...
1#1: start worker processes
...
```
Telecarto
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs ppr_prodige_telecarto_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Visualiseur
```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml logs ppr_prodige_visualiseur_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```

## Arrêt

```bash
docker-compose -f docker-compose.yml -f docker-compose.override.yml -f local_build.yml down
```
