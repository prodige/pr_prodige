# INSTALLATION DE PRODIGE SUR WINDOWS

Vidéo de présentation :

`https://www.prodige-opensource.org/upload/espace/1/video_installation_prodige_sur_windows.mp4`

## Prérequis WSL sur Windows

Installation de WSL sur Windows

Source : https://learn.microsoft.com/fr-fr/windows/wsl/install

- Ouvrez l’invite de commandes PowerShell ou Windows en mode administrateur en cliquant avec le bouton droit et en sélectionnant

« Exécuter en tant qu’administrateur »,

- Entrez la commande wsl --install, puis redémarrez votre machine.

PowerShell

```

wsl --install

```

Solution alternative :

Rechercher WSL dans le Microsoft Store et installer WSL via l'interface graphique

## Installation de Docker sur WSL

- Entrez les commandes sur WSL Linux pour installer Docker

Source : https://docs.docker.com/engine/install/ubuntu/

```

# Mise à jour des packages apt et installations des packets nécessaires pour utiliser le dépôt via HTTPS :

sudo apt-get update

sudo apt-get install \

ca-certificates \

curl \

gnupg \

lsb-release

# Ajout de la clé GPG officielle de Docker :

sudo mkdir -m 0755 -p /etc/apt/keyrings

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

# Configuration du dépôt :

echo \

"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \

$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# Mise à jour des packages apt et installation de Docker Engine, Containerd et Compose :

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin.

```

## Installation de Prodige sur WSL

Source : https://gitlab.adullact.net/prodige/pr_prodige/-/blob/master/README.md

- Entrez les commandes sur WSL Linux pour installer Prodige

```

# Téléchargement de l'archive de Prodige

wget https://gitlab.adullact.net/prodige/pr_prodige/-/archive/master/pr_prodige-master.tar

tar -xvf pr_prodige-master.tar

cd pr_prodige-master

# Installation de Prodige (Version de test avec comme URL : https://catalogue.prodige.internal)

sudo ./bin/make_init_and_rights.sh

```

Connexion au dépôt docker.alkante.com

Mot de passe : Yx@KN,ZXzR9u@

```

docker login docker.alkante.com -u prodige

docker-compose pull

docker-compose up -d prodige_database jpr_prodige_cas_ldap && docker-compose up -d



```

## Accèder à Prodige depuis Windows

- Ouvrez l’invite de commandes PowerShell ou Windows en mode administrateur en cliquant avec le bouton droit et en sélectionnant

« Exécuter en tant qu’administrateur »,

```



echo 127.0.0.2 cas.prodige.internal admincarto.prodige.internal adminsite.prodige.internal mapserv.prodige.internal catalogue.prodige.internal carto.prodige.internal datacarto.prodige.internal telecarto.prodige.internal print.prodige.internal table.prodige.internal >> C:\Windows\System32\drivers\etc\hosts



```

## Ouvrir Prodige dans un navigateur depuis Windows

- Entrer l'URL suivante :

URL : https://catalogue.prodige.internal

Login : `admin@prodige-opensource.org`
Mot de passe : `prodige01`
