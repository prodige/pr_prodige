# Installation de Prodige en mode production sans le docker proxy

## Principes

 - Utilisation de nginx comme proxy (sans docker)
 - Configuration des paramètres (dans les volumes de data).
 - Changement de l'URL par défaut par des scripts modifiant :
   - les paramètres ;
   - les données ;
   - les bases de données.

## Initalisation des volumes de données

 - data
 - var

En tant que `root` ou avec `sudo` :
```bash
sudo ./bin/make_init_and_rights.sh
```

## Nginx
Vous trouverez un configuration des vhosts nginx dans le dossier ./nginx/sites-available. Vous devez adapter les noms de domaines et le chemin des certificats

## Initialisation des paramètres
Cela concerne les mots de passes de bdd, la timezone, le relaie SMTP
```bash
sudo ./bin/set_prodige_parameters.sh
```

## Mise à jour des URL dans les paramètres

```bash
sudo ./bin/move_prodige_url_parameters.sh -d <your domain>
```

## Démarrage de Prodige (mode production)
- Utilisateur : prodige
- Mot de passe : Yx@KN,ZXzR9u@
```bash
docker login docker.alkante.com -u prodige
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml pull
```

Lancement des bases de données
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml up -d prodige_database jpr_prodige_cas_ldap
```
Lancement des applicatifs
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml up -d
```

## Journaux

```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs -f
```

## Vérification du bon démarrage
Les containers vont mettre un certain temps de démarrage. Voici des commandes pour vérifier qu'ils ont fini le démarrage.
### Base de données
BDD postgres :
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs prodige_database
...
LOG:  le système de bases de données est prêt pour accepter les connexions
```
BDD ldap :
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs jpr_prodige_cas_ldap
...
XXXXXX slapd starting
```

### App prodige
Admincarto (migration de bdd):
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs ppr_prodige_admincarto_web
...
++ finished in X.Xs
++ X migrations executed
++ XXXX sql queries
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Adminsite :
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs ppr_prodige_adminsite_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
CAS :
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs jpr_prodige_cas_web
...
- <Configuration files found at [/etc/cas/config] are [[/etc/cas/config/cas.properties]] under profile(s) [[standalone]]>
...
```
Catalogue (migration de bdd):
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs ppr_prodige_catalogue_web
...
++ finished in X.Xs
++ X migrations executed
++ XXXX sql queries
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Datacarto :
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs ppr_prodige_datacarto_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Geonetwork :
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs jpr_prodige_geonetwork_web
...
- Data directory provided could not be used. Using default location: /usr/local/tomcat/webapps/geonetwork/WEB-INF/data
...
```
Mapserver :
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs ppr_prodige_mapserver_api_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Proxy
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs prodige-proxy
...
1#1: start worker processes
...
```
Telecarto
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs ppr_prodige_telecarto_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```
Visualiseur
```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml logs ppr_prodige_visualiseur_web
...
ENTRYPOINT: Run apache : apache2ctl -D FOREGROUND
```

## Modification des données persistantes

```bash
sudo ./bin/move_prodige_url_data.sh -d <your domain>
```

## Modification des bases de données

```bash
sudo ./bin/move_prodige_url_db.sh -d <your domain>
```

## Arrêt

```bash
docker-compose -f docker-compose.yml -f nginx/docker-compose.nginx.yml -f production.yml down
```
