# Restauration des bases prodige et catalogue depuis des sauvegardes

IMPORTANT : pour faire un import, supprimer le volume de données `./var/database/data/` s'il n'est pas vide pour que les scripts initdb.d du Docker Postgresql s'exécutent au démarrage du conteneur.

Créer le répertoire de restauration **./var/database/restore/** avec `./bin/make_init_and_rights.sh`.

Copier les données de sauvegarde dans **./var/database/restore/** et préfixer les noms avec un chiffre **chiffre_** pour ordonner les imports.

Exemple :
- 0_PGSQL_privileges.dmp
- 1_PGSQL_PRODIGE.dmp
- 2_PGSQL_alk_respire.dmp

Si utilisés, les tablespaces doivent être dans le répertoire `/var/lib/postgresql/data` (tablespace par défaut du Docker).

Réexécuter `./bin/make_init_and_rights.sh` pour ajuster les droits.

Si le répertoire **./var/database/restore/** n'est pas vide (et que **./var/database/data/** l'est) les imports seront automtiquement chargés au démarrage du Docker dans l'ordre défini ci-dessus.

Ne démarrer que prodige_database :

```bash
docker-compose up prodige_database
```

Vérifier les journaux du conteneur :

```bash
docker-compose logs -t prodige_database
```

Vérifier que les bases ont été chargées :

```bash
docker exec -u postgres -t prodige_database psql -c '\l+'
```
