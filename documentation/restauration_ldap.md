# Restoration de LDAP à partir d'une sauvegarde

Créer les volumes et répertoires de restauration avec  `./bin/make_init_and_rights.sh` (en tant que `root`).

Copier les sauvegardes dans **./var/openldap/restore/** :

Exemples:
- LDAP_cn=config.ldif
- LDAP_dc=alkante,dc=domprodige.ldif

Si les volumes contiennent une mauvaise base de donnée LDAP les vider :

```bash
sudo bash -c "rm -rf var/openldap/database/* data/openldap/config/*"
```

Changer dans le docker-compose.yml :

```yml
...
    #command: ["--copy-service", "--loglevel", "warning"]
    # Restore
    entrypoint: sleep 30000
```

Démarrer seulement LDAP :
```bash
docker-compose -f docker-compose.yml up -d jpr_prodige_cas_ldap
```

Se connecter au conteneur et effectuer l'import manuellement :

```bash
docker exec -it jpr_prodige_cas_ldap bash
```
```bash
slapadd -F /etc/ldap/slapd.d -b cn=config -l /restore/LDAP_cn\=config.ldif
slapadd -F /etc/ldap/slapd.d -b dc=alkante,dc=domprodige -l /restore/LDAP_dc\=alkante\,dc\=domprodige.ldif
#fix path in ldif (if needed)
docker exec -it jpr_prodige_cas_ldap sed -i 's|olcTLSCACertificateFile:.*|olcTLSCACertificateFile: /container/run/service/slapd/assets/certs/ca.crt|' /etc/ldap/slapd.d/cn\=config.ldif
docker exec -it jpr_prodige_cas_ldap sed -i 's|olcTLSCertificateFile:.*|olcTLSCertificateFile: /container/run/service/slapd/assets/certs/ldap.crt|' /etc/ldap/slapd.d/cn\=config.ldif
docker exec -it jpr_prodige_cas_ldap sed -i 's|olcTLSCertificateKeyFile:.*|olcTLSCertificateKeyFile: /container/run/service/slapd/assets/certs/ldap.key|' /etc/ldap/slapd.d/cn\=config.ldif
docker exec -it jpr_prodige_cas_ldap sed -i 's|olcTLSDHParamFile:.*|olcTLSDHParamFile: /container/run/service/slapd/assets/certs/dhparam.pem|' /etc/ldap/slapd.d/cn\=config.ldif
#check
chown -R openldap:openldap /var/lib/ldap
chown -R openldap:openldap /etc/ldap
exit
```

Arrêt du conteneur :

```bash
docker-compose -f docker-compose.yml down
```

Créer le fichier suivant (fonctionnement interne du conteneur) :

```bash
echo 'export PREVIOUS_LDAP_TLS_CA_CRT_PATH=/container/run/service/slapd/assets/certs/ca.crt
export PREVIOUS_LDAP_TLS_CRT_PATH=/container/run/service/slapd/assets/certs/ldap.crt
export PREVIOUS_LDAP_TLS_KEY_PATH=/container/run/service/slapd/assets/certs/ldap.key
export PREVIOUS_LDAP_TLS_DH_PARAM_PATH=/container/run/service/slapd/assets/certs/dhparam.pem
' > data/openldap/config/docker-openldap-was-started-with-tls
```

Remettre le `docker-compose.yml` dans son état initial :

```yml
...
    command: ["--copy-service", "--loglevel", "warning"]
    # Restore
    #entrypoint: sleep 30000
```

Démarrer ensuite Prodige selon la méthode de déploiement choisie.
