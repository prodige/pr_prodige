# Génération de nouveaux certificats SSL

## Mise en place du certificat
### 1. Utilisation de certificat existant
Prodige possède de multiple URL, vous devez donc avoir un certificat de type wildcard

2 fichiers doivent être présent dans ./data/proxy/ssl
- prodige.crt : chain de certification + certificat
- prodige.key : clé du certificat

### 2. Utilisation de certificat auto-signé
La plateforme en configurer par défaut pour le domaine `*.prodige.internal`, vous n'avez rien à faire si cela vous convient.

Sinon voici la procédure
```bash
FILENAME="prodige"
CERT_CN="*.prodige.internal" # à changer si besoin
openssl genrsa 2048 | sudo tee ./data/proxy/ssl/${FILENAME}.key > /dev/null
openssl req -new -x509 -nodes -sha1 -days 3650 -key ./data/proxy/ssl/${FILENAME}.key -subj "/C=FR/CN=$CERT_CN" | sudo tee ./data/proxy/ssl/${FILENAME}.crt
```

Dans ce cas vous de devez pas utiliser le fichier production.yml lors d'une commande docker-compose

## Trustore/keystore java
### Création d'un `truststore` (contient le crt de confiance pour la connexion du client ssl)

```bash
sudo rm ./data/cas/keystore/truststore.jks
sudo keytool -import -trustcacerts -alias prodige -file ./data/proxy/ssl/${FILENAME}.crt -keystore ./data/cas/keystore/truststore.jks -storepass secret
sudo chmod 666 ./data/cas/keystore/truststore.jks
```

#### Tester le truststore.jks
```bash
keytool -list -v -alias prodige -keystore ./data/cas/keystore/truststore.jks
```

### Créer un `keystore` (contient `key` et `crt` pour la connexion au serveur SSL)

```bash
sudo rm ./data/proxy/ssl/${FILENAME}.p12
sudo openssl pkcs12 -export -in ./data/proxy/ssl/${FILENAME}.crt -inkey ./data/proxy/ssl/${FILENAME}.key   -out ./data/proxy/ssl/${FILENAME}.p12 -name prodige -password pass:secret
```

#### Vérifications

```bash
sudo keytool -list -v -keystore ./data/proxy/ssl/${FILENAME}.p12 \
    -storepass secret \
    -storetype PKCS12
```

#### Conversion de p12 à jks :

```bash
sudo keytool -importkeystore -srckeystore ./data/proxy/ssl/${FILENAME}.p12 -srcstoretype pkcs12 -srcalias prodige -srcstorepass secret -destkeystore ./data/cas/keystore/keystore.jks -deststoretype jks -deststorepass secret  -destalias prodige
```

#### Vérification du `keystore`

```bash
keytool -list -v -alias prodige -keystore ./data/cas/keystore/keystore.jks -storepass secret
```
