# PRODIGE

PRODIGE est une infrastructure de données permettant de cataloguer des données, de produire des services (cartes interactives notamment) et de diffuser les données au bénéfice de partenaires et du grand public.

Son architecture modulaire est motivée par l'attention portée à sa capacité à évoluer et par son approche open source, lui permettant de bénéficier des apports de la communauté active des utilisateurs.

Prodige bénéficie d'une gouvernance partagée favorisant ainsi un développement cohérent et consensuel de l'outil. Les instances participatives (Comité de coordination, Comité de pilotage, Club utilisateurs ...) sont des lieux d’échange et de partage qui permettent de prendre en compte les besoins de chacun au bénéfice de la communauté.

La documentation de l'outil et les informations essentielles de la communauté sont accessibles sur [Prodige opensource](https://www.prodige-opensource.org).

# Documentation

 - [Accès à la documentation fonctionnelle](https://www.prodige-opensource.org/accueil/documentation)
 - [Accès aux tutoriels](https://www.prodige-opensource.org/accueil/tutoriels)
 - [plateformes utilisant PRODIGE](https://www.prodige-opensource.org/accueil/qui-sommes-nous-)
 
# Installation de Prodige (Docker)

Les documentations sont dans le répertoire [documentation](documentation/README.md).

# Licence

[informations relatives à la licence](LICENSE.md)